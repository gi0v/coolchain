#ifndef _CANG_BACKEND_H
#define _CANG_BACKEND_H

#include <cang.h>
#include <casm.h>
#include <cm_base.h>

void cang_push_frame(Casm *casm);
void cang_pop_frame(Casm *casm);
void cang_compile_program(Cang *cang, Casm *casm);
void cang_compile_stmt(Cang *cang, Casm *casm, CangNode *stmt);
bool cang_native_proc_check(Casm *casm, String name);
CangTypeDef cang_compile_expr(Cang *cang, Casm *casm, CangNode *expr,
                              CangCompilerRequest req, bool *contains_global_vars, size_t rec_depth);

#endif  // _CANG_BACKEND_H
