#ifndef _CANG_AST_H
#define _CANG_AST_H

#include <stddef.h>

#include <strlib.h>
#include <file_location.h>

typedef struct {
  void **items;
  size_t item_sz;
  size_t items_count;
} CangList;

typedef enum {
  NODE_KIND_START,
  NODE_EXPR_KIND_ID,
  NODE_EXPR_KIND_NUM,
  NODE_EXPR_KIND_STR,
  NODE_EXPR_KIND_ADD,
  NODE_EXPR_KIND_SUB,
  NODE_EXPR_KIND_EQU,
  NODE_EXPR_KIND_NEQ,
  NODE_EXPR_KIND_GRE,
  NODE_EXPR_KIND_LES,
  NODE_EXPR_KIND_GEQ,
  NODE_EXPR_KIND_LEQ,
  NODE_EXPR_KIND_MUL,
  NODE_EXPR_KIND_DIV,
  NODE_EXPR_KIND_MOD,
  NODE_EXPR_KIND_BW_OR,
  NODE_EXPR_KIND_BW_AND,
  NODE_EXPR_KIND_BW_XOR,
  NODE_EXPR_KIND_BW_SHL,
  NODE_EXPR_KIND_BW_SHR,
  NODE_EXPR_KIND_INDEX,
  NODE_EXPR_KIND_DEREF,
  NODE_EXPR_KIND_ADDR_OF,
  NODE_EXPR_KIND_PROCEDURAL,
  NODE_STMT_KIND_COMPOUND,
  NODE_STMT_KIND_NOP,
  NODE_STMT_KIND_EXPR,
  NODE_STMT_KIND_PROC_DEF,
  NODE_STMT_KIND_RETURN,
  NODE_STMT_KIND_IF,
  NODE_STMT_KIND_WHILE,
  NODE_STMT_KIND_FOR,
  NODE_STMT_KIND_VAR_DEF,
  NODE_STMT_KIND_SET,
  NODE_STMT_KIND_ADD_SET,
  NODE_STMT_KIND_SUB_SET,
  NODE_STMT_KIND_MUL_SET,
  NODE_STMT_KIND_DIV_SET,
  NODE_STMT_KIND_BREAK,
  NODE_STMT_KIND_CONTINUE,
  NODE_KIND_PROC_ARGS,
} CangNodeKind;

typedef struct {
  bool has_precedence;
  String value;
  CangList *children;
  CangNodeKind kind;
  FileLocation location;
} CangNode;

CangList *cang_list_new(size_t item_sz);
void *cang_list_add(CangList *list, void *item);
CangNode *cang_node_new(CangNodeKind kind, FileLocation location);
void cang_node_free(CangNode *node);
CangNode *cang_node_dup(CangNode *node, FileLocation location);
void cang_single_node_free(CangNode *node);
CangNode *cang_add_node_child(CangNode *parent, CangNode *child);
CangNode *cang_get_node_child(CangNode *node, size_t index);
void cang_merge_nodes(CangNode *dst, CangNode *src);

#endif  // _CANG_AST_H
