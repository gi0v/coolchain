#ifndef _CANG_LEXER_H
#define _CANG_LEXER_H

#include <stdbool.h>

#include <cang.h>

typedef enum {
  TOK_UNKNOWN = 0,
  TOK_ID,
  TOK_EOF,
  TOK_NUM_LIT,
  TOK_CHR_LIT,
  TOK_STR_LIT,
  TOK_PROC,
  TOK_RET,
  TOK_EQU,
  TOK_NEQ,
  TOK_GRE,
  TOK_LES,
  TOK_GEQ,
  TOK_LEQ,
  TOK_SHL,
  TOK_SHR,
  TOK_IF,
  TOK_ELSE,
  TOK_VAR,
  TOK_FOR,
  TOK_BREAK,
  TOK_CONTINUE,
  TOK_WHILE,
  TOK_ADD_EQ,
  TOK_SUB_EQ,
  TOK_MUL_EQ,
  TOK_DIV_EQ,
  TOK_MOD_EQ,
  TOK_ADD = '+',
  TOK_SUB = '-',
  TOK_DIV = '/',
  TOK_MOD = '%',
  TOK_EOL = ';',
  TOK_SET = '=',
  TOK_TSET = ':',
  TOK_PIPE = '|',
  TOK_CFLEX = '^',
  TOK_COMMA = ',',
  TOK_LCURLY = '{',
  TOK_RCURLY = '}',
  TOK_LPAREN = '(',
  TOK_RPAREN = ')',
  TOK_PREPROC = '@',
  TOK_ASTERISK = '*',
  TOK_LBRACKET = '[',
  TOK_RBRACKET = ']',
  TOK_AMPERSAND = '&',
  TOK_COMMENT = '#',
} CangTokenKind;

typedef struct {
  String value;
  CangTokenKind kind;
} CangToken;

CangLexer *cang_lexer_new(const char *input_file);
void cang_lexer_free(CangLexer *lexer);
void cang_free_token(CangToken tok);
CangToken cang_next_token(CangLexer *lexer);
CangToken cang_expect_token(CangLexer *lexer, CangTokenKind kind);
CangToken cang_peek_token(CangLexer *lexer);
const char *cang_token_kind_to_cstr(CangTokenKind kind);
String cang_str_token(CangToken tok);
void cang_dump_token(CangToken tok, bool eol);

#endif  // _CANG_LEXER_H
