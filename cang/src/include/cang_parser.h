#ifndef _CANG_PARSER_H
#define _CANG_PARSER_H

#include <cang.h>
#include <cang_ast.h>

const char *cang_node_kind_to_cstr(CangNodeKind kind);
void cang_print_depth(FILE * restrict stream, size_t depth);
void cang_dump_ast(FILE * restrict stream, CangNode *start, size_t depth);
CangNode *cang_parse_proc_args(CangLexer *lexer);
void cang_parse_program(Cang *cang);
CangNode *cang_parse_file(CangLexer *lexer);
CangNode *cang_parse_stmt(CangNode *ast, CangLexer *lexer);
CangNode *cang_parse_expr(CangLexer *lexer, bool precedence);
CangNode *cang_parse_term(CangLexer *lexer);

#endif  // _CANG_PARSER_H
