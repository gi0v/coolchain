#ifndef _CANG_H
#define _CANG_H

#include <stddef.h>

#include <strlib.h>
#include <cm_base.h>
#include <cang_ast.h>
#include <file_location.h>

typedef void* ToFree;

typedef enum {
  SCOPE_PROC    = 1 << 0,
  SCOPE_LOOP    = 1 << 1,
  SCOPE_GENERIC = 1 << 2,
} CangScopeKind;

typedef enum {
  REQ_NONE          = 1 << 0,
  REQ_PUSH_VAR_ADDR = 1 << 2,
} CangCompilerRequest;

typedef enum {
  TYPE_VOID = 0,
  TYPE_U8   = 1 << 0,
  TYPE_CHAR = 1 << 0,
  TYPE_U16  = 1 << 1,
  TYPE_U32  = 1 << 2,
  TYPE_U64  = 1 << 3,
  TYPE_VOID_PTR  = (1 << 3) + 1,
  TYPE_U8_PTR  = (1 << 3) + 2,
  TYPE_CHAR_PTR  = (1 << 3) + 2,
  TYPE_U16_PTR  = (1 << 3) + 3,
  TYPE_U32_PTR  = (1 << 3) + 4,
  TYPE_U64_PTR  = (1 << 3) + 5,
} CangType;

typedef struct {
  CmWord read, write;
} CangTypeDefAsInstr;

typedef struct {
  CangType ptr, non_ptr;
  CangTypeDefAsInstr instr;
} CangTypeDefAs;

typedef struct {
  CangType type;
  CangTypeDefAs as;
} CangTypeDef;

typedef struct {
  String name;
  bool is_global; // => rel_addr is the absolute address.
  CmAddr rel_addr; // NOTE: Do not use this address! Get the absolute one with cang_push_var_addr() instead.
  CangTypeDef type;
} CangVariable;

typedef struct {
  String name;
  CangNode *value;
} CangConstant;

typedef struct {
  String name;
  size_t arity;
  CangTypeDef return_type;
  CangTypeDef *args_types;
} CangProcDef;

typedef struct _CANG_SCOPE CangScope;

struct _CANG_SCOPE {
  CangScope *parent;
  CangScopeKind kind;
  CmAddr rel_mem_addr;
  size_t variables_count;
  CangVariable *variables;
};

typedef struct {
  String src;
  FileLocation inp_file;
} CangLexer;

typedef struct {
  CmAddr abs_mem_addr;
  CangNode *ast;
  CangScope *scope;
  CangLexer *lexer;
  CangProcDef *proc_defs;
  size_t proc_defs_count;
  const char *out_file;
  size_t gvbc_sz;
  CmWord *global_vars_bc;
} Cang;

extern ToFree *To_Free;
extern size_t To_Free_Count;
extern CangConstant *constants;
extern size_t constants_count;

extern CangTypeDef type_to_type_def[];
extern char *type_to_cstr[];

CangTypeDef cang_str_to_type_def(CangNode *node, String str);
CmWord *global_vars_bc_new();
void global_vars_bc_free(CmWord *bc);
void global_vars_bc_push_bc(Cang *cang, CmWord val);
void global_vars_bc_push_instr(Cang *cang, CmInstrType instr, CmWord param);
void to_free_init();
void to_free_free();
void to_free_push(ToFree addr);
CangNode *cang_constants_find(String name);
void cang_constants_push(String name, CangNode *value);
CangProcDef *cang_proc_defs_new();
bool cang_is_proc_def(Cang *cang, String name);
CangProcDef *cang_add_proc_def(Cang *cang, String name, size_t arity, CangTypeDef return_type);
void cang_add_proc_arg_type(Cang *cang, CangTypeDef type, size_t i);
CangScope *cang_scope_new();
Cang *cang_new();
void cang_free(Cang *cang);

#endif  // _CANG_H
