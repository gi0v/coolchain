#include <cang.h>
#include <casm_ops.h>
#include <error_logger.h>

ToFree *To_Free;
size_t To_Free_Count = 0;
CangConstant *Constants;
size_t Constants_Count = 0;

CangTypeDef type_to_type_def[] = {
  [TYPE_U8]       = { .type = TYPE_U8,       .as = { .ptr = TYPE_U8_PTR,   .non_ptr = TYPE_U8,   .instr = { .read = INSTR_RD8,  .write = INSTR_WR8  }}},
  [TYPE_U16]      = { .type = TYPE_U16,      .as = { .ptr = TYPE_U16_PTR,  .non_ptr = TYPE_U16,  .instr = { .read = INSTR_RD16, .write = INSTR_WR16 }}},
  [TYPE_U32]      = { .type = TYPE_U32,      .as = { .ptr = TYPE_U32_PTR,  .non_ptr = TYPE_U32,  .instr = { .read = INSTR_RD32, .write = INSTR_WR32 }}},
  [TYPE_U64]      = { .type = TYPE_U64,      .as = { .ptr = TYPE_U64_PTR,  .non_ptr = TYPE_U64,  .instr = { .read = INSTR_RD64, .write = INSTR_WR64 }}},
  [TYPE_VOID]     = { .type = TYPE_VOID,     .as = { .ptr = TYPE_VOID_PTR, .non_ptr = TYPE_VOID, .instr = { .read = INSTR_NOP,  .write = INSTR_NOP  }}},
  [TYPE_U8_PTR]   = { .type = TYPE_U8_PTR,   .as = { .ptr = TYPE_U8_PTR,   .non_ptr = TYPE_U8,   .instr = { .read = INSTR_RD64, .write = INSTR_WR64 }}},
  [TYPE_U16_PTR]  = { .type = TYPE_U16_PTR,  .as = { .ptr = TYPE_U16_PTR,  .non_ptr = TYPE_U16,  .instr = { .read = INSTR_RD64, .write = INSTR_WR64 }}},
  [TYPE_U32_PTR]  = { .type = TYPE_U32_PTR,  .as = { .ptr = TYPE_U32_PTR,  .non_ptr = TYPE_U32,  .instr = { .read = INSTR_RD64, .write = INSTR_WR64 }}},
  [TYPE_U64_PTR]  = { .type = TYPE_U64_PTR,  .as = { .ptr = TYPE_U64_PTR,  .non_ptr = TYPE_U64,  .instr = { .read = INSTR_RD64, .write = INSTR_WR64 }}},
  [TYPE_VOID_PTR] = { .type = TYPE_VOID_PTR, .as = { .ptr = TYPE_VOID_PTR, .non_ptr = TYPE_VOID, .instr = { .read = INSTR_RD64, .write = INSTR_WR64 }}},
};

char *type_to_cstr[] = {
  [TYPE_U8]       = "uint8",
  [TYPE_U16]      = "uint16",
  [TYPE_U32]      = "uint32",
  [TYPE_U64]      = "uint64",
  [TYPE_VOID]     = "void",
  [TYPE_U8_PTR]   = "uint8_ptr",
  [TYPE_U16_PTR]  = "uint16_ptr",
  [TYPE_U32_PTR]  = "uint32_ptr",
  [TYPE_U64_PTR]  = "uint64_ptr",
  [TYPE_VOID_PTR] = "void_ptr",
};

CangTypeDef cang_str_to_type_def(CangNode *node, String str)
{
  if (STR_CSTR_CMP(str, "uint8")
      || STR_CSTR_CMP(str, "char"))          return type_to_type_def[TYPE_U8];
  else if (STR_CSTR_CMP(str, "uint16"))      return type_to_type_def[TYPE_U16];
  else if (STR_CSTR_CMP(str, "uint32"))      return type_to_type_def[TYPE_U32];
  else if (STR_CSTR_CMP(str, "uint64"))      return type_to_type_def[TYPE_U64];
  else if (STR_CSTR_CMP(str, "void"))        return type_to_type_def[TYPE_VOID];
  else if (STR_CSTR_CMP(str, "uint8_ptr")
           || STR_CSTR_CMP(str, "char_ptr")) return type_to_type_def[TYPE_U8_PTR];
  else if (STR_CSTR_CMP(str, "uint16_ptr"))  return type_to_type_def[TYPE_U16_PTR];
  else if (STR_CSTR_CMP(str, "uint32_ptr"))  return type_to_type_def[TYPE_U32_PTR];
  else if (STR_CSTR_CMP(str, "uint64_ptr"))  return type_to_type_def[TYPE_U64_PTR];
  else if (STR_CSTR_CMP(str, "void_ptr"))    return type_to_type_def[TYPE_VOID_PTR];
  else log_compiling_error(node->location, "unknown type name: `"STRFmt"`.", STRArg(str));
}

CmWord *global_vars_bc_new()
{
  CmWord *bc = calloc(1, sizeof(*bc));
  return bc;
}

void global_vars_bc_free(CmWord *bc)
{
  free(bc);
}

void global_vars_bc_push_bc(Cang *cang, CmWord val)
{
  cang->gvbc_sz++;
  cang->global_vars_bc = realloc(cang->global_vars_bc, sizeof(CmWord) * (cang->gvbc_sz + 1));
  cang->global_vars_bc[cang->gvbc_sz - 1] = val;
}

void global_vars_bc_push_instr(Cang *cang, CmInstrType instr, CmWord param)
{
  global_vars_bc_push_bc(cang, (CmWord) instr);
  global_vars_bc_push_bc(cang, param);
}

void to_free_init()
{
  To_Free = calloc(1, sizeof(ToFree *));
}

void to_free_free()
{
  size_t i;

  for (i = 0; i < To_Free_Count; ++i)
    free(To_Free[i]);

  free(To_Free);
}

void to_free_push(ToFree addr)
{
  To_Free_Count++;
  To_Free = realloc(To_Free, sizeof(ToFree *) * (To_Free_Count + 1));
  To_Free[To_Free_Count - 1] = addr;
}

CangNode *cang_constants_find(String name)
{
  size_t i;

  for (i = 0; i < Constants_Count; ++i)
    if (STR_CMP(Constants[i].name, name))
      return Constants[i].value;

  return NULL;
}

void cang_constants_push(String name, CangNode *value)
{
  Constants_Count++;
  Constants = realloc(Constants, sizeof(CangConstant) * (Constants_Count + 1));
  Constants[Constants_Count - 1].name = name;
  Constants[Constants_Count - 1].value = value;
}

void cang_constants_free()
{
  size_t i;

  for (i = 0; i < Constants_Count; ++i) {
    str_free(Constants[i].name);
    cang_node_free(Constants[i].value);
  }

  free(Constants);
}

CangLexer *cang_lexer_new(const char *input_file)
{
  CangLexer *lexer = calloc(1, sizeof(*lexer));

  lexer->inp_file = file_location_new(input_file);

  if (!str_read_file(STR((char *)input_file), &lexer->src))
    log_io_error("there was an error opening / reading from file: `%s`", input_file);

  return lexer;
}

void cang_lexer_free(CangLexer *lexer)
{
  str_free(lexer->src);
  free(lexer);
}

CangProcDef *cang_proc_defs_new()
{
  CangProcDef *proc_defs = calloc(1, sizeof(*proc_defs));
  return proc_defs;
}

void cang_proc_defs_free(CangProcDef *proc_defs, size_t proc_defs_count)
{
  size_t i;

  for (i = 0; i < proc_defs_count; ++i)
    free(proc_defs[i].args_types);

  free(proc_defs);
}

bool cang_is_proc_def(Cang *cang, String name)
{
  size_t i;

  for (i = 0; i < cang->proc_defs_count; ++i)
    if (STR_CMP(cang->proc_defs[i].name, name))
      return true;

  return false;
}

CangProcDef *cang_add_proc_def(Cang *cang, String name, size_t arity, CangTypeDef return_type)
{
  if (cang_is_proc_def(cang, name)) return NULL;

  cang->proc_defs_count++;
  cang->proc_defs = realloc(cang->proc_defs, sizeof(CangProcDef) * (cang->proc_defs_count + 1));
  cang->proc_defs[cang->proc_defs_count - 1].name = name;
  cang->proc_defs[cang->proc_defs_count - 1].arity = arity;
  cang->proc_defs[cang->proc_defs_count - 1].args_types = calloc(arity + 1, sizeof(CangTypeDef));
  cang->proc_defs[cang->proc_defs_count - 1].return_type = return_type;

  return &cang->proc_defs[cang->proc_defs_count - 1];
}

void cang_add_proc_arg_type(Cang *cang, CangTypeDef type, size_t i)
{ cang->proc_defs[cang->proc_defs_count - 1].args_types[i] = type; }

CangScope *cang_scope_new()
{
  CangScope *scope;

  scope = calloc(1, sizeof(*scope));
  scope->parent = NULL;
  scope->variables_count = 0;
  scope->variables = calloc(1, sizeof(*scope->variables));

  return scope;
}

void cang_full_scope_free(CangScope *scope)
{
  CangScope *tmp;

  while (scope != NULL) {
    tmp = scope->parent;

    free(scope->variables);
    free(scope);
    scope = tmp;
  }
}

Cang *cang_new(const char *inp_file, const char *out_file)
{
  Cang *cang = calloc(1, sizeof(*cang));

  to_free_init();
  Constants = calloc(1, sizeof(CangConstant));

  cang->lexer = cang_lexer_new(inp_file);
  cang->out_file = out_file;
  cang->scope = cang_scope_new();
  cang->proc_defs = cang_proc_defs_new();
  cang->proc_defs_count = 0;
  cang->abs_mem_addr = MEM_CAP - 1;
  cang->global_vars_bc = global_vars_bc_new();
  cang->gvbc_sz = 0;

  // Native procedures.
  cang_add_proc_def(cang, STR("dump"), 1, type_to_type_def[TYPE_VOID]);
  cang_add_proc_arg_type(cang, type_to_type_def[TYPE_U64], 0);
  cang_add_proc_def(cang, STR("write"), 1, type_to_type_def[TYPE_VOID]);
  cang_add_proc_arg_type(cang, type_to_type_def[TYPE_VOID_PTR], 0);
  cang_add_proc_def(cang, STR("getchar"), 0, type_to_type_def[TYPE_CHAR]);
  cang_add_proc_def(cang, STR("abort"), 0, type_to_type_def[TYPE_VOID]);
  cang_add_proc_def(cang, STR("load_ptr"), 2, type_to_type_def[TYPE_U64]);
  cang_add_proc_def(cang, STR("store_ptr"), 3, type_to_type_def[TYPE_VOID]);
  cang_add_proc_def(cang, STR("cast"), 2, type_to_type_def[TYPE_VOID]);

  return cang;
}

void cang_free(Cang *cang)
{
  to_free_free(To_Free, To_Free_Count);
  cang_constants_free();
  cang_node_free(cang->ast);
  cang_lexer_free(cang->lexer);
  cang_full_scope_free(cang->scope);
  global_vars_bc_free(cang->global_vars_bc);
  cang_proc_defs_free(cang->proc_defs, cang->proc_defs_count);
  free(cang);
}
