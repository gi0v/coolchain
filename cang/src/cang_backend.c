#include <assert.h>
#include <stdint.h>

#include <casm.h>
#include <cang.h>
#include <casm_ops.h>
#include <casm_labels.h>
#include <cang_parser.h>
#include <cang_backend.h>
#include <error_logger.h>

#define CURRENT_FRAME_GDME 0x0
#define STACK_SIZE_GDME    0x8

#define READ_CURRENT_FRAME(casm)                               \
  do {                                                         \
    casm_push_instr(casm, INSTR_PUSH, CURRENT_FRAME_GDME);     \
    casm_push_instr(casm, INSTR_RD64, 0);                      \
  } while (false)                                              \

#define READ_STACK_SIZE(casm)                                  \
  do {                                                         \
    casm_push_instr(casm, INSTR_PUSH, STACK_SIZE_GDME);        \
    casm_push_instr(casm, INSTR_RD64, 0);                      \
  } while (false)                                              \

#define INCREMENT_STACK_SIZE(casm, n)                   \
  do {                                                  \
    casm_push_instr(casm, INSTR_PUSH, STACK_SIZE_GDME); \
    READ_STACK_SIZE(casm);                              \
    casm_push_instr(casm, INSTR_PUSH, n);               \
    casm_push_instr(casm, INSTR_ADD, 0);                \
    casm_push_instr(casm, INSTR_WR64, 0);               \
  } while (false)                                       \

void cang_compile_program(Cang *cang, Casm *casm)
{
  size_t i;

  // Global frame setup, along with global metadata
  {
    // Setup current frame GDME:
    casm_push_instr(casm, INSTR_PUSH, 0);
    casm_push_instr(casm, INSTR_PUSH, 16);
    casm_push_instr(casm, INSTR_WR64, 0);
    
    // Setup stack size GDME:
    casm_push_instr(casm, INSTR_PUSH, 8);
    casm_push_instr(casm, INSTR_PUSH, 24);
    casm_push_instr(casm, INSTR_WR64, 0);
    
    // Setup previous frame LDME for global frame:
    casm_push_instr(casm, INSTR_PUSH, 16);
    casm_push_instr(casm, INSTR_PUSH, 16);
    casm_push_instr(casm, INSTR_WR64, 0);
  }

#define MAIN_CALL_ADDR 49

  // main()
  cang_push_frame(casm);
  {
    casm_push_instr(casm, INSTR_PUSH, 0); // <-- Place holder
    casm_push_instr(casm, INSTR_CALL, 0);
  }
  cang_pop_frame(casm);

  casm_push_instr(casm, INSTR_HLT, 0);

  for (i = 0; i < cang->ast->children->items_count; ++i)
    cang_compile_stmt(cang, casm, cang_get_node_child(cang->ast, i));
}

void cang_push_frame(Casm *casm)
{
  // 1. Read current frame address and write it to the address stack size points to:
  READ_STACK_SIZE(casm);
  READ_CURRENT_FRAME(casm);
  casm_push_instr(casm, INSTR_WR64, 0);

  // 2. Read stack size and write it to current frame address GDME:
  casm_push_instr(casm, INSTR_PUSH, CURRENT_FRAME_GDME);
  READ_STACK_SIZE(casm);
  casm_push_instr(casm, INSTR_WR64, 0);

  // 3. Increment the stack size by 8 to take account of the local metadata:
  INCREMENT_STACK_SIZE(casm, 8);
}

void cang_pop_frame(Casm *casm)
{
  // 1. Read current frame address
  READ_CURRENT_FRAME(casm);

  // 2. Read previous frame address and write it to the current frame GMDE
  casm_push_instr(casm, INSTR_PUSH, CURRENT_FRAME_GDME);
  READ_CURRENT_FRAME(casm);
  casm_push_instr(casm, INSTR_RD64, 0);
  casm_push_instr(casm, INSTR_WR64, 0);

  // 3. Write (previously read) current frame address to the stack size
  casm_push_instr(casm, INSTR_PUSH, STACK_SIZE_GDME);
  casm_push_instr(casm, INSTR_SWAP, 0);
  casm_push_instr(casm, INSTR_WR64, 0);
}

void cang_push_scope(Cang *cang, CangScopeKind kind)
{
  CangScope *new_scope;
  
  new_scope = cang_scope_new();
  new_scope->kind = kind;
  new_scope->parent = cang->scope;
  cang->scope = new_scope;
}

void cang_pop_scope(Cang *cang)
{
  CangScope *tmp;
  
  tmp = cang->scope;
  cang->scope = cang->scope->parent;
  free(tmp->variables);
  free(tmp);
}

bool cang_is_inside_loop(Cang *cang)
{
  CangScope *tmp;

  tmp = cang->scope;
  
  while (tmp->parent != NULL) {
    if (tmp->kind & SCOPE_LOOP) return true;

    tmp = tmp->parent;
  }

  return false;
}

#define IS_UINT(type) (type >= TYPE_U8 && type <= TYPE_U64)
#define IS_UINT_PTR(type) (type >= TYPE_U8_PTR && type <= TYPE_U64_PTR)

bool is_type_acceptable(CangType exp_type, CangType type)
{
  if (exp_type == type) return true;
  else if (IS_UINT(exp_type) && IS_UINT(type)) return !(exp_type < type);
  else if (IS_UINT_PTR(exp_type) && IS_UINT_PTR(type)) return !(exp_type < type);
  else if ((exp_type == TYPE_VOID) || (type == TYPE_VOID)) return false;
  else if (((exp_type == TYPE_VOID_PTR) && IS_UINT_PTR(type))
           || ((type == TYPE_VOID_PTR) && IS_UINT_PTR(exp_type))) return true;
  else if ((exp_type == TYPE_VOID_PTR) || (type == TYPE_VOID_PTR)) return false;
  else return false;
}

CangTypeDef cang_type_check_expr(Cang *cang, Casm *casm, CangNode *expr,
                                 CangCompilerRequest req, bool *cgvs, CangType expected_type, size_t rec_depth)
{
  CangTypeDef type = cang_compile_expr(cang, casm, expr, req, cgvs, rec_depth);

  if (!is_type_acceptable(expected_type, type.type))
    log_compiling_error(expr->location,
                        "was expecting type `%s`, but got `%s`.",
                        type_to_cstr[expected_type], type_to_cstr[type.type]);

  return type;
}

CangProcDef cang_str_to_proc_def(Cang *cang, CangNode *node)
{
  size_t i;

  for (i = 0; i < cang->proc_defs_count; ++i)
    if (STR_CMP(cang->proc_defs[i].name, node->value))
      return cang->proc_defs[i];

  log_compiling_error(node->location, "procedure not found: "STRFmt".", STRArg(node->value));
}

CangType cang_num_to_min_type(uint64_t num)
{
  if (num > UINT8_MAX) {
    if (num > UINT16_MAX) {
      if (num > UINT32_MAX) {
        if (num > UINT64_MAX) {
          assert(false && "Unreachable.");
        } else return TYPE_U64;
      } else return TYPE_U32;
    } else return TYPE_U16;
  } else return TYPE_U8;
}

CangScope *cang_get_closest_frame_relevant_scope(Cang *cang)
{
  CangScope *tmp;

  tmp = cang->scope;

  while (tmp->parent != NULL && !(tmp->kind & SCOPE_PROC)) tmp = tmp->parent;

  return tmp;
}

CangVariable *cang_search_variable(Cang *cang, String name)
{
  size_t i;
  CangScope *tmp;
  
  tmp = cang->scope;
  
  while (tmp != NULL) {
    for (i = 0; i < tmp->variables_count; ++i)
      if (STR_CMP(tmp->variables[i].name, name))
        return &tmp->variables[i];
    
    tmp = tmp->parent;
  }
  
  return NULL;
}


CangVariable cang_push_compile_time_variable(Cang *cang, Casm *casm, CangNode *node, String var_name, String type)
{
  CangScope *tmp;
  CangTypeDef var_type = cang_str_to_type_def(node, type);

  if (var_name.length > 1)
    if (cang_search_variable(cang, var_name) != NULL)
      log_compiling_error(node->location, "variable "STRFmt" is already declared.", STRArg(var_name));

  cang->scope->variables_count++;
  cang->scope->variables = realloc(cang->scope->variables, sizeof(CangVariable) * cang->scope->variables_count + 1);
  cang->scope->variables[cang->scope->variables_count - 1].name = var_name;
  cang->scope->variables[cang->scope->variables_count - 1].type = var_type;

  if (cang->scope->parent == NULL) {
    cang->scope->variables[cang->scope->variables_count - 1].is_global = true;

    if (var_type.type >= TYPE_VOID_PTR) cang->abs_mem_addr -= var_type.type - (var_type.type - TYPE_VOID_PTR) - 1;
    else cang->abs_mem_addr -= var_type.type;

    cang->scope->variables[cang->scope->variables_count - 1].rel_addr = cang->abs_mem_addr;
  } else {
    cang->scope->variables[cang->scope->variables_count - 1].is_global = false;
    tmp = cang_get_closest_frame_relevant_scope(cang);

    cang->scope->variables[cang->scope->variables_count - 1].rel_addr = tmp->rel_mem_addr;

    if (var_type.type >= TYPE_VOID_PTR) {
      tmp->rel_mem_addr += var_type.type - (var_type.type - TYPE_VOID_PTR) - 1;
      INCREMENT_STACK_SIZE(casm, var_type.type - (var_type.type - TYPE_VOID_PTR) - 1);
    } else { tmp->rel_mem_addr += var_type.type; INCREMENT_STACK_SIZE(casm, var_type.type); }
  }

  return cang->scope->variables[cang->scope->variables_count - 1];
}

void cang_push_dyn_glob_var_addr(Cang *cang, CangVariable var)
{
  global_vars_bc_push_instr(cang, INSTR_PUSH, var.rel_addr);
}

void cang_push_var_addr(Casm *casm, CangVariable var)
{
  if (var.is_global) {
    casm_push_instr(casm, INSTR_PUSH, var.rel_addr);
  } else {
    READ_CURRENT_FRAME(casm);
    casm_push_instr(casm, INSTR_PUSH, var.rel_addr);
    casm_push_instr(casm, INSTR_ADD, 0);
    casm_push_instr(casm, INSTR_PUSH, 8); // This is done to account for the frame metadata
    casm_push_instr(casm, INSTR_ADD, 0);
  }
}

void cang_push_compile_time_str_lit(Cang *cang, Casm *casm, String str)
{
  CmWord c;
  size_t i;
  CangScope *tmp;

  tmp = cang_get_closest_frame_relevant_scope(cang);

  // Push string address
  READ_CURRENT_FRAME(casm);
  casm_push_instr(casm, INSTR_PUSH, tmp->rel_mem_addr);
  casm_push_instr(casm, INSTR_ADD, 0);
  casm_push_instr(casm, INSTR_PUSH, 8);
  casm_push_instr(casm, INSTR_ADD, 0);

  for (i = 0; i <= str.length; ++i) {
    c = (CmWord) str_get_char(str, i);

    READ_CURRENT_FRAME(casm);
    casm_push_instr(casm, INSTR_PUSH, tmp->rel_mem_addr++);
    casm_push_instr(casm, INSTR_ADD, 0);
    casm_push_instr(casm, INSTR_PUSH, 8);
    casm_push_instr(casm, INSTR_ADD, 0);

    casm_push_instr(casm, INSTR_PUSH, c);
    casm_push_instr(casm, INSTR_WR8, 0);
  }

  INCREMENT_STACK_SIZE(casm, str.length);
}

void cang_compile_stmt(Cang *cang, Casm *casm, CangNode *stmt)
{
  switch (stmt->kind) {
  case NODE_STMT_KIND_PROC_DEF: {
    size_t i;
    CmWord addr;
    CangNode *proc = cang_get_node_child(stmt, 0), *proc_args;
    CangNode *body = cang_get_node_child(stmt, 1);

    if (proc->children->items_count < 3)
      log_compiling_error(proc->location, "unproper proc definition.");

    cang_push_scope(cang, SCOPE_PROC);
    {
      if (proc->kind == NODE_EXPR_KIND_PROCEDURAL) {
        addr = casm->current_addr;
        proc_args = cang_get_node_child(proc, 1);
        casm_push_label(casm, cang_get_node_child(proc, 0)->value);
        
        if (STR_CSTR_CMP(cang_get_node_child(proc, 0)->value, "main"))
          casm->bytecode[MAIN_CALL_ADDR] = addr;
        
        if (proc_args->kind == NODE_KIND_PROC_ARGS) {
          if (cang_add_proc_def(cang,
                                cang_get_node_child(proc, 0)->value,
                                proc_args->children->items_count,
                                cang_str_to_type_def(cang_get_node_child(proc, 2),
                                                  cang_get_node_child(proc, 2)->value)) == NULL)
            log_compiling_error(proc->location, "redeclaration of procedure `"STRFmt"`",
                                STRArg(cang_get_node_child(proc, 0)->value));
          
          if (proc_args->children->items_count > 0) {
            for (i = proc_args->children->items_count - 1; i + 1 > 0; --i) {
              CangNode *arg = cang_get_node_child(proc_args, i);
              CangNode *arg_type = cang_get_node_child(arg, 0);
              CangVariable var = cang_push_compile_time_variable(cang, casm, arg_type, arg->value, arg_type->value);
              
              casm_push_instr(casm, INSTR_SWAP, 0);
              cang_push_var_addr(casm, var);
              casm_push_instr(casm, INSTR_SWAP, 0);
              casm_push_instr(casm, var.type.as.instr.write, 0);
              
              cang_add_proc_arg_type(cang, cang_str_to_type_def(arg_type, arg_type->value), i);
            }
          }
        }
      }

      if (STR_CSTR_CMP(cang_get_node_child(proc, 0)->value, "main")) {
        size_t i;

        for (i = 0; i < cang->gvbc_sz; ++i)
          casm_push_bytecode(casm, cang->global_vars_bc[i]);
      }

      cang_compile_stmt(cang, casm, body);
      casm_push_instr(casm, INSTR_RET, 0);
    }
    cang_pop_scope(cang);
  } break;
  case NODE_STMT_KIND_NOP: {
    casm_push_instr(casm, INSTR_NOP, 0);
  } break;
  case NODE_STMT_KIND_RETURN: {
    if (cang_is_inside_loop(cang)) {
      casm_push_instr(casm, INSTR_DROP, 0);
      casm_push_instr(casm, INSTR_DROP, 0);
    }

    if (stmt->children->items_count > 0) {
      cang_type_check_expr(cang,
                           casm,
                           cang_get_node_child(stmt, 0),
                           REQ_NONE,
                           NULL,
                           cang->proc_defs[cang->proc_defs_count - 1].return_type.type, 1);
      casm_push_instr(casm, INSTR_SWAP, 0);
    }

    casm_push_instr(casm, INSTR_RET, 0);
  } break;
  case NODE_STMT_KIND_IF: {
    CmAddr then_jmp_addr, else_jmp_addr, else_addr;

    cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_NONE, NULL, 1);
    casm_push_instr(casm, INSTR_NOT, 0);

    then_jmp_addr = casm->current_addr;

    casm_push_instr(casm, INSTR_PUSH, 0);
    casm_push_instr(casm, INSTR_CJMP, 0);

    cang_compile_stmt(cang, casm, cang_get_node_child(stmt, 1));

    if (stmt->children->items_count == 2) {
      casm->bytecode[then_jmp_addr + 1] = casm->current_addr;
    } else if (stmt->children->items_count == 3) {
      else_jmp_addr = casm->current_addr;

      casm_push_instr(casm, INSTR_PUSH, 0);
      casm_push_instr(casm, INSTR_JMP, 0);

      else_addr = casm->current_addr;

      cang_compile_stmt(cang, casm, cang_get_node_child(stmt, 2));

      casm->bytecode[then_jmp_addr + 1] = else_addr;
      casm->bytecode[else_jmp_addr + 1] = casm->current_addr;
    }
  } break;
  case NODE_STMT_KIND_WHILE: {
    CmAddr loop_start, push_loop_end, push_push_loop_end;

    loop_start = casm->current_addr;
    cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_NONE, NULL, 1);

    casm_push_instr(casm, INSTR_NOT, 0);
    push_loop_end = casm->current_addr;
    casm_push_instr(casm, INSTR_PUSH, 0); // Place-holder
    casm_push_instr(casm, INSTR_CJMP, 0);

    casm_push_instr(casm, INSTR_PUSH, loop_start);
    push_push_loop_end = casm->current_addr;
    casm_push_instr(casm, INSTR_PUSH, 0); // Place-holder

    cang_push_scope(cang, SCOPE_LOOP);
    {
      cang_compile_stmt(cang, casm, cang_get_node_child(stmt, 1));
    }
    cang_pop_scope(cang);

    casm_push_instr(casm, INSTR_DROP, 0);
    casm_push_instr(casm, INSTR_DROP, 0);

    casm_push_instr(casm, INSTR_PUSH, loop_start);
    casm_push_instr(casm, INSTR_JMP, 0);

    casm->bytecode[push_loop_end + 1] = casm->bytecode[push_push_loop_end + 1] = casm->current_addr;
  } break;
  case NODE_STMT_KIND_FOR: {
    CmAddr loop_start, push_loop_end, push_push_loop_end;

    cang_compile_stmt(cang, casm, cang_get_node_child(stmt, 0));

    loop_start = casm->current_addr;
    cang_compile_expr(cang, casm, cang_get_node_child(stmt, 1), REQ_NONE, NULL, 1);

    casm_push_instr(casm, INSTR_NOT, 0);
    push_loop_end = casm->current_addr;
    casm_push_instr(casm, INSTR_PUSH, 0); // Place-holder
    casm_push_instr(casm, INSTR_CJMP, 0);

    casm_push_instr(casm, INSTR_PUSH, loop_start);
    push_push_loop_end = casm->current_addr;
    casm_push_instr(casm, INSTR_PUSH, 0); // Place-holder

    cang_push_scope(cang, SCOPE_LOOP);
    {
      cang_compile_stmt(cang, casm, cang_get_node_child(stmt, 3));
      cang_compile_stmt(cang, casm, cang_get_node_child(stmt, 2));
    }
    cang_pop_scope(cang);

    casm_push_instr(casm, INSTR_DROP, 0);
    casm_push_instr(casm, INSTR_DROP, 0);

    casm_push_instr(casm, INSTR_PUSH, loop_start);
    casm_push_instr(casm, INSTR_JMP, 0);

    casm->bytecode[push_loop_end + 1] = casm->bytecode[push_push_loop_end + 1] = casm->current_addr;
  } break;
  case NODE_STMT_KIND_VAR_DEF: {
    size_t i;

    if (cang_get_node_child(stmt, 1)->kind != NODE_EXPR_KIND_ID)
      log_compiling_error(cang_get_node_child(stmt, 1)->location, "was expecting an identifier for type.");

    if (cang_get_node_child(stmt, 0)->kind == NODE_EXPR_KIND_ID)
      cang_push_compile_time_variable(cang, casm,
                                      cang_get_node_child(stmt, 1),
                                      cang_get_node_child(stmt, 0)->value,
                                      cang_get_node_child(stmt, 1)->value);
    else if (cang_get_node_child(stmt, 0)->kind == NODE_EXPR_KIND_INDEX) {
      if (cang_get_node_child(cang_get_node_child(stmt, 0), 1)->kind != NODE_EXPR_KIND_NUM)
        log_compiling_error(cang_get_node_child(cang_get_node_child(stmt, 0), 1)->location,
                            "array count needs to be a number literal.");

      uint64_t count = strtoull(STR_CSTR(cang_get_node_child(cang_get_node_child(stmt, 0), 1)->value), NULL, 10);

      if (count <= 1) log_compiling_error(cang_get_node_child(cang_get_node_child(stmt, 0), 1)->location,
                                          "array count needs to be higher than 1.");

      CangVariable first_var = cang_push_compile_time_variable(cang, casm,
                                                               cang_get_node_child(stmt, 1),
                                                               STR(""),
                                                               cang_get_node_child(stmt, 1)->value);

      for (i = 1; i < count; ++i)
        cang_push_compile_time_variable(cang, casm,
                                        cang_get_node_child(stmt, 1),
                                        STR(""),
                                        cang_get_node_child(stmt, 1)->value);

      if (cang->scope->parent == NULL) {
        cang_push_dyn_glob_var_addr(cang,
                                    cang_push_compile_time_variable(cang, casm,
                                                                    cang_get_node_child(cang_get_node_child(stmt, 0), 0),
                                                                    cang_get_node_child(cang_get_node_child(stmt, 0), 0)
                                                                    ->value, STR(type_to_cstr[cang_str_to_type_def(cang_get_node_child(stmt, 1), cang_get_node_child(stmt, 1)->value).as.ptr])));
        cang_push_dyn_glob_var_addr(cang, first_var);
        global_vars_bc_push_instr(cang, INSTR_WR64, 0);
      } else {
        cang_push_var_addr(casm,
                           cang_push_compile_time_variable(cang, casm,
                                                           cang_get_node_child(cang_get_node_child(stmt, 0), 0),
                                                           cang_get_node_child(cang_get_node_child(stmt, 0), 0)
                                                           ->value, STR(type_to_cstr[cang_str_to_type_def(cang_get_node_child(stmt, 1), cang_get_node_child(stmt, 1)->value).as.ptr])));
        cang_push_var_addr(casm, first_var);
        casm_push_instr(casm, INSTR_WR64, 0);
      }


    } else log_compiling_error(cang_get_node_child(stmt, 0)->location,
                               "unexpected expression when compiling variable definition.");
  } break;
  case NODE_STMT_KIND_SET: {
    CangTypeDef type;
    type = cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_PUSH_VAR_ADDR, NULL, 1);
    cang_type_check_expr(cang, casm, cang_get_node_child(stmt, 1), REQ_NONE, NULL, type.type, 1);
    casm_push_instr(casm, type.as.instr.write, 0);
  } break;
  case NODE_STMT_KIND_ADD_SET: {
    CangTypeDef type;
    type = cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_PUSH_VAR_ADDR, NULL, 1);
    casm_push_instr(casm, INSTR_DUP, 0);
    casm_push_instr(casm, type.as.instr.read, 0);
    cang_type_check_expr(cang, casm, cang_get_node_child(stmt, 1), REQ_NONE, NULL, type.type, 1);
    casm_push_instr(casm, INSTR_ADD, 0);
    casm_push_instr(casm, type.as.instr.write, 0);
  } break;
  case NODE_STMT_KIND_SUB_SET: {
    CangTypeDef type;
    type = cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_PUSH_VAR_ADDR, NULL, 1);
    casm_push_instr(casm, INSTR_DUP, 0);
    casm_push_instr(casm, type.as.instr.read, 0);
    cang_type_check_expr(cang, casm, cang_get_node_child(stmt, 1), REQ_NONE, NULL, type.type, 1);
    casm_push_instr(casm, INSTR_SUB, 0);
    casm_push_instr(casm, type.as.instr.write, 0);
  } break;
  case NODE_STMT_KIND_MUL_SET: {
    CangTypeDef type;
    type = cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_PUSH_VAR_ADDR, NULL, 1);
    casm_push_instr(casm, INSTR_DUP, 0);
    casm_push_instr(casm, type.as.instr.read, 0);
    cang_type_check_expr(cang, casm, cang_get_node_child(stmt, 1), REQ_NONE, NULL, type.type, 1);
    casm_push_instr(casm, INSTR_MUL, 0);
    casm_push_instr(casm, type.as.instr.write, 0);
  } break;
  case NODE_STMT_KIND_DIV_SET: {
    CangTypeDef type;
    type = cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_PUSH_VAR_ADDR, NULL, 1);
    casm_push_instr(casm, INSTR_DUP, 0);
    casm_push_instr(casm, type.as.instr.read, 0);
    cang_type_check_expr(cang, casm, cang_get_node_child(stmt, 1), REQ_NONE, NULL, type.type, 1);
    casm_push_instr(casm, INSTR_DIV, 0);
    casm_push_instr(casm, type.as.instr.write, 0);
  } break;
  case NODE_STMT_KIND_COMPOUND: {
    size_t i;
    
    cang_push_scope(cang, SCOPE_GENERIC);
    {
      for (i = 0; i < stmt->children->items_count; ++i)
        cang_compile_stmt(cang, casm, cang_get_node_child(stmt, i));
    }
    cang_pop_scope(cang);
  } break;
  case NODE_STMT_KIND_EXPR: {
    cang_compile_expr(cang, casm, cang_get_node_child(stmt, 0), REQ_NONE, NULL, 0);
  } break;
  case NODE_STMT_KIND_BREAK: {
    if (!cang_is_inside_loop(cang))
      log_compiling_error(stmt->location, "break used outside of a loop.");

    casm_push_instr(casm, INSTR_SWAP, 0);
    casm_push_instr(casm, INSTR_DROP, 0);
    casm_push_instr(casm, INSTR_JMP, 0);
  } break;
  case NODE_STMT_KIND_CONTINUE: {
    if (!cang_is_inside_loop(cang))
      log_compiling_error(stmt->location, "continue used outside of a loop.");

    casm_push_instr(casm, INSTR_DROP, 0);
    casm_push_instr(casm, INSTR_JMP, 0);
  } break;
  default:
    assert(false && "Unreachable.");
  }
}

bool cang_native_proc_check(Casm *casm, String name)
{
  if (STR_CSTR_CMP(name, "dump")) casm_push_instr(casm, INSTR_NAT, NATIVE_DUMP);
  else if (STR_CSTR_CMP(name, "write")) casm_push_instr(casm, INSTR_NAT, NATIVE_WRITE);
  else if (STR_CSTR_CMP(name, "abort")) casm_push_instr(casm, INSTR_NAT, NATIVE_ABORT);
  else if (STR_CSTR_CMP(name, "getchar")) casm_push_instr(casm, INSTR_NAT, NATIVE_READ);
  else return false;
  
  return true;
}

CangTypeDef cang_compile_expr(Cang *cang, Casm *casm, CangNode *expr,
                              CangCompilerRequest req, bool *contains_glob_vars, size_t rec_depth)
{
  size_t i;
  CangTypeDef max = type_to_type_def[TYPE_VOID];
  CangTypeDef tmp;
  
  if (expr == NULL) return max;
  
  switch (expr->kind) {
  case NODE_EXPR_KIND_DEREF: {
    if (rec_depth == 0) return max;

    max = type_to_type_def[cang_compile_expr(cang, casm, cang_get_node_child(expr, 0), REQ_NONE, NULL, rec_depth + 1).as.non_ptr];

    if (!(req & REQ_PUSH_VAR_ADDR))
      casm_push_instr(casm, max.as.instr.read, 0);
  } return max;
  case NODE_EXPR_KIND_ADDR_OF: {
    if (rec_depth == 0) return max;

    max = type_to_type_def[cang_compile_expr(cang, casm, cang_get_node_child(expr, 0), REQ_PUSH_VAR_ADDR, NULL, rec_depth + 1).as.ptr];
  } return max;
  case NODE_EXPR_KIND_PROCEDURAL: {
    CangNode *proc_args = cang_get_node_child(expr, 1);
    CangProcDef proc_def = cang_str_to_proc_def(cang, cang_get_node_child(expr, 0));

    if (proc_def.arity != proc_args->children->items_count)
      log_compiling_error(expr->location,
                          "invalid arity: %zu, actual procedure arity: %zu.",
                          proc_args->children->items_count,
                          cang_str_to_proc_def(cang, cang_get_node_child(expr, 0)).arity);

    if (STR_CSTR_CMP(proc_def.name, "cast")) {
      cang_compile_expr(cang, casm, cang_get_node_child(proc_args, 0), req, NULL, rec_depth + 1);
      return cang_str_to_type_def(cang_get_node_child(proc_args, 1), cang_get_node_child(proc_args, 1)->value);
    } else if (STR_CSTR_CMP(proc_def.name, "store_ptr")) {
      cang_type_check_expr(cang, casm, cang_get_node_child(proc_args, 0), req, NULL, TYPE_VOID_PTR, rec_depth + 1);
      cang_type_check_expr(cang, casm, cang_get_node_child(proc_args, 1), req, NULL, TYPE_U64, rec_depth + 1);
      casm_push_instr(casm,
                      cang_str_to_type_def(cang_get_node_child(proc_args, 2),
                                           cang_get_node_child(proc_args, 2)->value).as.instr.write, 0);
      return type_to_type_def[TYPE_VOID];
    } else if (STR_CSTR_CMP(proc_def.name, "load_ptr")) {
      cang_type_check_expr(cang, casm, cang_get_node_child(proc_args, 0), req, NULL, TYPE_VOID_PTR, rec_depth + 1);
      casm_push_instr(casm,
                      cang_str_to_type_def(cang_get_node_child(proc_args, 1),
                                           cang_get_node_child(proc_args, 1)->value).as.instr.read, 0);
      return cang_str_to_type_def(cang_get_node_child(proc_args, 1), cang_get_node_child(proc_args, 1)->value);
    }

    max = proc_def.return_type;

    for (i = 0; i < proc_args->children->items_count; ++i)
      cang_type_check_expr(cang, casm, cang_get_node_child(proc_args, i), req, NULL, proc_def.args_types[i].type, rec_depth + 1);
    
    if (!cang_native_proc_check(casm, cang_get_node_child(expr, 0)->value)) {
      cang_push_frame(casm);
      {
        casm_push_instr(casm, INSTR_PUSH, casm_get_label_addr(casm, cang_get_node_child(expr, 0)->value));
        casm_push_instr(casm, INSTR_CALL, 0);
      }
      cang_pop_frame(casm);
    }

    if (rec_depth == 0 && proc_def.return_type.type != TYPE_VOID)
      casm_push_instr(casm, INSTR_DROP, 0);

  } return max;
  case NODE_EXPR_KIND_INDEX: {
    if (rec_depth == 0) return max;

    bool is_global = false;
    max = type_to_type_def[cang_type_check_expr(cang,
                                                casm,
                                                cang_get_node_child(expr, 0),
                                                REQ_NONE,
                                                &is_global,
                                                TYPE_VOID_PTR,
                                                rec_depth + 1
                                                ).as.non_ptr];

    cang_compile_expr(cang, casm, cang_get_node_child(expr, 1), REQ_NONE, NULL, rec_depth + 1);
    casm_push_instr(casm, INSTR_PUSH, max.type);
    casm_push_instr(casm, INSTR_MUL, 0); // (array_index * sizeof(array_type))

    if (is_global) casm_push_instr(casm, INSTR_SUB, 0);
    else casm_push_instr(casm, INSTR_ADD, 0);

    if (!(req & REQ_PUSH_VAR_ADDR))
      casm_push_instr(casm, max.as.instr.read, 0);
  } return max;
  case NODE_EXPR_KIND_NUM: {
    if (rec_depth == 0) return max;

    max = type_to_type_def[cang_num_to_min_type(strtoull(STR_CSTR(expr->value), NULL, 10))];

    casm_push_instr(casm, INSTR_PUSH, strtoull(STR_CSTR(expr->value), NULL, 10));
  } return max;
  case NODE_EXPR_KIND_ID: {
    if (rec_depth == 0) return max;

    CangVariable *var = cang_search_variable(cang, expr->value);
    
    if (var == NULL)
      log_compiling_error(expr->location, "variable `"STRFmt"` not found in the current scope.", STRArg(expr->value));

    cang_push_var_addr(casm, *var);

    max = var->type;

    if (contains_glob_vars != NULL) *contains_glob_vars = var->is_global;

    if (!(req & REQ_PUSH_VAR_ADDR))
      casm_push_instr(casm, var->type.as.instr.read, 0);
  } return max;
  case NODE_EXPR_KIND_STR: {
    if (rec_depth == 0) return max;

    max = type_to_type_def[TYPE_CHAR_PTR];

    cang_push_compile_time_str_lit(cang, casm, expr->value);
  } return max;
  default:; // C Hates me, so I gotta do this.
  }

  // At this point, we know this is a binary expression and not anything else.

  if (rec_depth == 0) return max;

  max = cang_compile_expr(cang, casm, cang_get_node_child(expr, 0), req, NULL, rec_depth + 1);
  tmp = cang_compile_expr(cang, casm, cang_get_node_child(expr, 1), req, NULL, rec_depth + 1);

  if (!is_type_acceptable(max.type, tmp.type))
    max = tmp;

  switch (expr->kind) {
  case NODE_EXPR_KIND_ADD: casm_push_instr(casm, INSTR_ADD, 0); break;
  case NODE_EXPR_KIND_SUB: casm_push_instr(casm, INSTR_SUB, 0); break;
  case NODE_EXPR_KIND_MUL: casm_push_instr(casm, INSTR_MUL, 0); break;
  case NODE_EXPR_KIND_DIV: casm_push_instr(casm, INSTR_DIV, 0); break;
  case NODE_EXPR_KIND_MOD: casm_push_instr(casm, INSTR_MOD, 0); break;
  case NODE_EXPR_KIND_EQU: casm_push_instr(casm, INSTR_EQU, 0); break;
  case NODE_EXPR_KIND_NEQ: casm_push_instr(casm, INSTR_NEQ, 0); break;
  case NODE_EXPR_KIND_GRE: casm_push_instr(casm, INSTR_GRE, 0); break;
  case NODE_EXPR_KIND_LES: casm_push_instr(casm, INSTR_LES, 0); break;
  case NODE_EXPR_KIND_GEQ: casm_push_instr(casm, INSTR_GEQ, 0); break;
  case NODE_EXPR_KIND_LEQ: casm_push_instr(casm, INSTR_LEQ, 0); break;
  case NODE_EXPR_KIND_BW_OR: casm_push_instr(casm, INSTR_OR, 0); break;
  case NODE_EXPR_KIND_BW_AND: casm_push_instr(casm, INSTR_AND, 0); break;
  case NODE_EXPR_KIND_BW_XOR: casm_push_instr(casm, INSTR_XOR, 0); break;
  case NODE_EXPR_KIND_BW_SHL: casm_push_instr(casm, INSTR_SHL, 0); break;
  case NODE_EXPR_KIND_BW_SHR: casm_push_instr(casm, INSTR_SHR, 0); break;
  default:
    assert(false && "Unreachable.");
  }

  return max;
}
