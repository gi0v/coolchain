#include <assert.h>

#include <cang_ast.h>
#include <cang_lexer.h>
#include <cang_parser.h>
#include <error_logger.h>

const char *cang_node_kind_to_cstr(CangNodeKind kind)
{
  switch (kind) {
  case NODE_KIND_START: return "NODE_KIND_START";
  case NODE_EXPR_KIND_ID: return "NODE_EXPR_KIND_ID";
  case NODE_EXPR_KIND_NUM: return "NODE_EXPR_KIND_NUM";
  case NODE_EXPR_KIND_STR: return "NODE_EXPR_KIND_STR";
  case NODE_EXPR_KIND_ADD: return "NODE_EXPR_KIND_ADD";
  case NODE_EXPR_KIND_SUB: return "NODE_EXPR_KIND_SUB";
  case NODE_EXPR_KIND_EQU: return "NODE_EXPR_KIND_EQU";
  case NODE_EXPR_KIND_NEQ: return "NODE_EXPR_KIND_NEQ";
  case NODE_EXPR_KIND_GRE: return "NODE_EXPR_KIND_GRE";
  case NODE_EXPR_KIND_LES: return "NODE_EXPR_KIND_LES";
  case NODE_EXPR_KIND_GEQ: return "NODE_EXPR_KIND_GEQ";
  case NODE_EXPR_KIND_LEQ: return "NODE_EXPR_KIND_LEQ";
  case NODE_EXPR_KIND_MUL: return "NODE_EXPR_KIND_MUL";
  case NODE_EXPR_KIND_DIV: return "NODE_EXPR_KIND_DIV";
  case NODE_EXPR_KIND_MOD: return "NODE_EXPR_KIND_MOD";
  case NODE_EXPR_KIND_BW_OR: return "NODE_EXPR_KIND_BW_OR";
  case NODE_EXPR_KIND_BW_AND: return "NODE_EXPR_KIND_BW_AND";
  case NODE_EXPR_KIND_BW_XOR: return "NODE_EXPR_KIND_BW_XOR";
  case NODE_EXPR_KIND_BW_SHL: return "NODE_EXPR_KIND_BW_SHL";
  case NODE_EXPR_KIND_BW_SHR: return "NODE_EXPR_KIND_BW_SHR";
  case NODE_EXPR_KIND_INDEX: return "NODE_EXPR_KIND_INDEX";
  case NODE_EXPR_KIND_DEREF: return "NODE_EXPR_KIND_DEREF";
  case NODE_EXPR_KIND_ADDR_OF: return "NODE_EXPR_KIND_ADDR_OF";
  case NODE_EXPR_KIND_PROCEDURAL: return "NODE_EXPR_KIND_PROCEDURAL";
  case NODE_STMT_KIND_COMPOUND: return "NODE_STMT_KIND_COMPOUND";
  case NODE_STMT_KIND_NOP: return "NODE_STMT_KIND_NOP";
  case NODE_STMT_KIND_EXPR: return "NODE_STMT_KIND_EXPR";
  case NODE_STMT_KIND_PROC_DEF: return "NODE_STMT_KIND_PROC_DEF";
  case NODE_STMT_KIND_RETURN: return "NODE_STMT_KIND_RETURN";
  case NODE_STMT_KIND_IF: return "NODE_STMT_KIND_IF";
  case NODE_STMT_KIND_WHILE: return "NODE_STMT_KIND_WHILE";
  case NODE_STMT_KIND_FOR: return "NODE_STMT_KIND_FOR";
  case NODE_STMT_KIND_VAR_DEF: return "NODE_STMT_KIND_VAR_DEF";
  case NODE_KIND_PROC_ARGS: return "NODE_KIND_PROC_ARGS";
  case NODE_STMT_KIND_SET: return "NODE_STMT_KIND_SET";
  case NODE_STMT_KIND_ADD_SET: return "NODE_STMT_KIND_ADD_SET";
  case NODE_STMT_KIND_SUB_SET: return "NODE_STMT_KIND_SUB_SET";
  case NODE_STMT_KIND_MUL_SET: return "NODE_STMT_KIND_MUL_SET";
  case NODE_STMT_KIND_DIV_SET: return "NODE_STMT_KIND_DIV_SET";
  case NODE_STMT_KIND_BREAK: return "NODE_STMT_KIND_BREAK";
  case NODE_STMT_KIND_CONTINUE: return "NODE_STMT_KIND_CONTINUE";
  }

  return NULL;
}

void cang_print_depth(FILE * restrict stream, size_t depth)
{
  size_t i;

  if (depth == 0) {
    fprintf(stream, "|- ");
    return;
  }

  for (i = 0; i < depth; ++i)
    fprintf(stream, "|  ");

  fprintf(stream, "`- ");
}

void cang_dump_ast(FILE * restrict stream, CangNode *start, size_t depth)
{
  size_t i;

  if (start == NULL)
    return;

  cang_print_depth(stream, depth);
  if (start->value.length)
    fprintf(stream, "%s (`"STRFmt"`)\n", cang_node_kind_to_cstr(start->kind), STRArg(start->value));
  else
    fprintf(stream, "%s\n", cang_node_kind_to_cstr(start->kind));

  for (i = 0; i < start->children->items_count; ++i)
    cang_dump_ast(stream, cang_get_node_child(start, i), depth + 1);
}

CangNode *cang_parse_proc_args(CangLexer *lexer)
{
  CangNode *proc_args;
  CangToken token;

  proc_args = cang_node_new(NODE_KIND_PROC_ARGS, lexer->inp_file);

  token = cang_peek_token(lexer);

  if (token.kind == TOK_RPAREN) {
    cang_free_token(token);
    return proc_args;
  } else cang_free_token(token);

  cang_add_node_child(proc_args, cang_parse_expr(lexer, false));

  token = cang_peek_token(lexer);

  while (token.kind == TOK_COMMA) {
    cang_free_token(cang_expect_token(lexer, token.kind));
    cang_add_node_child(proc_args, cang_parse_expr(lexer, false));

    cang_free_token(token);
    token = cang_peek_token(lexer);
  }

  cang_free_token(token);

  return proc_args;
}


CangNode *cang_parse_term(CangLexer *lexer)
{
  CangNode *ret = NULL, *tmp = NULL;
  CangToken tok = cang_next_token(lexer), peek;

  switch (tok.kind) {
  case TOK_ASTERISK:
    ret = cang_node_new(NODE_EXPR_KIND_DEREF, lexer->inp_file);

    cang_add_node_child(ret, cang_parse_term(lexer));
    cang_free_token(tok);
    break;
  case TOK_AMPERSAND:
    ret = cang_node_new(NODE_EXPR_KIND_ADDR_OF, lexer->inp_file);

    cang_add_node_child(ret, cang_parse_term(lexer));
    cang_free_token(tok);
    break;
  case TOK_ID:
    ret = cang_node_new(NODE_EXPR_KIND_ID, lexer->inp_file);
    ret->value = tok.value;
    ret->has_precedence = true;

    if ((peek = cang_peek_token(lexer)).kind == TOK_LPAREN) {
      cang_free_token(cang_expect_token(lexer, peek.kind));

      tmp = cang_node_new(NODE_EXPR_KIND_PROCEDURAL, lexer->inp_file);
      cang_add_node_child(tmp, ret);
      cang_add_node_child(tmp, cang_parse_proc_args(lexer));
      ret = tmp;

      cang_free_token(cang_expect_token(lexer, TOK_RPAREN));
    } else if (peek.kind == TOK_LBRACKET) {
      cang_free_token(cang_expect_token(lexer, peek.kind));

      tmp = cang_node_new(NODE_EXPR_KIND_INDEX, lexer->inp_file);
      cang_add_node_child(tmp, ret);
      cang_add_node_child(tmp, cang_parse_expr(lexer, false));
      ret = tmp;

      cang_free_token(cang_expect_token(lexer, TOK_RBRACKET));
    } else if (cang_constants_find(tok.value) != NULL) {
      CangNode *val;

      val = cang_constants_find(tok.value);
      cang_node_free(ret);
      ret = cang_node_dup(val, ret->location);
    } cang_free_token(peek);

    break;
  case TOK_STR_LIT:
    ret = cang_node_new(NODE_EXPR_KIND_STR, lexer->inp_file);
    ret->value = tok.value;
    ret->has_precedence = true;
    break;
  case TOK_NUM_LIT:
    ret = cang_node_new(NODE_EXPR_KIND_NUM, lexer->inp_file);
    ret->value = tok.value;
    ret->has_precedence = true;
    break;
  case TOK_CHR_LIT:
    ret = cang_node_new(NODE_EXPR_KIND_NUM, lexer->inp_file);
    ret->value = str_new();
    ret->has_precedence = true;

    FMT_APPEND_TO_STR(ret->value, "%d", str_get_char(tok.value, 0));
    cang_free_token(tok);
    break;
  case TOK_LPAREN:
    cang_free_token(tok);
    ret = cang_parse_expr(lexer, true);

    if (ret != NULL)
      cang_free_token(cang_expect_token(lexer, TOK_RPAREN));

    break;
  default:
    log_compiling_error(lexer->inp_file, "unexpected token: %s", cang_str_token(tok));
  }

  return ret;
}

CangNode *cang_parse_expr(CangLexer *lexer, bool precedence)
{
  CangNode *a, *b, *tmp;
  CangToken tok_tmp;
  CangTokenKind peek;

  a = cang_parse_term(lexer);

  while (true) {
    tok_tmp = cang_peek_token(lexer);
    peek = tok_tmp.kind;
    cang_free_token(tok_tmp);

    switch (peek) {
    case TOK_ADD:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_ADD, lexer->inp_file);
      b = cang_parse_term(lexer);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_SUB:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_SUB, lexer->inp_file);
      b = cang_parse_term(lexer);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_ASTERISK:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_MUL, lexer->inp_file);
      b = cang_parse_term(lexer);

      if (a->has_precedence) {
        cang_add_node_child(tmp, a);
        cang_add_node_child(tmp, b);
        a = tmp;
      } else {
        CangNode *child = cang_get_node_child(a, 1);

        cang_add_node_child(tmp, child);
        cang_add_node_child(tmp, b);

        a->children->items[1] = tmp;
      }

      break;
    case TOK_DIV:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_DIV, lexer->inp_file);
      b = cang_parse_term(lexer);

      if (a->has_precedence) {
        cang_add_node_child(tmp, a);
        cang_add_node_child(tmp, b);
        a = tmp;
      } else {
        CangNode *child = cang_get_node_child(a, 1);

        cang_add_node_child(tmp, child);
        cang_add_node_child(tmp, b);

        a->children->items[1] = tmp;
      }

      break;
    case TOK_MOD:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_MOD, lexer->inp_file);
      b = cang_parse_term(lexer);

      if (a->has_precedence) {
        cang_add_node_child(tmp, a);
        cang_add_node_child(tmp, b);
        a = tmp;
      } else {
        CangNode *child = cang_get_node_child(a, 1);

        cang_add_node_child(tmp, child);
        cang_add_node_child(tmp, b);

        a->children->items[1] = tmp;
      }

      break;
    case TOK_AMPERSAND:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_BW_AND, lexer->inp_file);
      b = cang_parse_term(lexer);

      if (a->has_precedence) {
        cang_add_node_child(tmp, a);
        cang_add_node_child(tmp, b);
        a = tmp;
      } else {
        CangNode *child = cang_get_node_child(a, 1);

        cang_add_node_child(tmp, child);
        cang_add_node_child(tmp, b);

        a->children->items[1] = tmp;
      }

      break;
    case TOK_PIPE:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_BW_OR, lexer->inp_file);
      b = cang_parse_term(lexer);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_CFLEX:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_BW_XOR, lexer->inp_file);
      b = cang_parse_term(lexer);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_SHL:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_BW_SHL, lexer->inp_file);
      b = cang_parse_term(lexer);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_SHR:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_BW_SHR, lexer->inp_file);
      b = cang_parse_term(lexer);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_EQU:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_EQU, lexer->inp_file);
      b = cang_parse_expr(lexer, false);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_NEQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_NEQ, lexer->inp_file);
      b = cang_parse_expr(lexer, false);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_GRE:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_GRE, lexer->inp_file);
      b = cang_parse_expr(lexer, false);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_LES:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_LES, lexer->inp_file);
      b = cang_parse_expr(lexer, false);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_GEQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_GEQ, lexer->inp_file);
      b = cang_parse_expr(lexer, false);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_LEQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_EXPR_KIND_LEQ, lexer->inp_file);
      b = cang_parse_expr(lexer, false);
      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_SET:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_STMT_KIND_SET, lexer->inp_file);
      b = cang_parse_expr(lexer, false);

      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_ADD_EQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_STMT_KIND_ADD_SET, lexer->inp_file);
      b = cang_parse_expr(lexer, false);

      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_SUB_EQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_STMT_KIND_SUB_SET, lexer->inp_file);
      b = cang_parse_expr(lexer, false);

      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_MUL_EQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_STMT_KIND_MUL_SET, lexer->inp_file);
      b = cang_parse_expr(lexer, false);

      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_DIV_EQ:
      cang_free_token(cang_expect_token(lexer, peek));

      tmp = cang_node_new(NODE_STMT_KIND_DIV_SET, lexer->inp_file);
      b = cang_parse_expr(lexer, false);

      cang_add_node_child(tmp, a);
      cang_add_node_child(tmp, b);
      a = tmp;

      break;
    case TOK_TSET:
      cang_free_token(cang_expect_token(lexer, peek));
      cang_add_node_child(a, cang_parse_term(lexer));

      break;
    default:
      a->has_precedence = precedence;
      return a;
    }
  }
}

CangNode *cang_parse_stmt(CangNode *ast, CangLexer *lexer)
{
  CangNode *node, *tmp;
  CangToken token;

  token = cang_peek_token(lexer);

  switch (token.kind) {
  case TOK_PREPROC: {
    // TODO: Add macros.

    CangToken identifier;

    cang_free_token(cang_expect_token(lexer, token.kind));

    identifier = cang_expect_token(lexer, TOK_ID);

    if (STR_CSTR_CMP(identifier.value, "include")) {
      size_t i, j = 0;
      CangNode *incl_ast;
      CangToken incl_file = cang_expect_token(lexer, TOK_STR_LIT);
      CangLexer *incl_lexer;

      // TODO: Should probably add some actual procedures to get path from file name, because
      // this implementation sucks.
      if (str_find_char_right(STR((char *) lexer->inp_file.file), '/', &j)) {
        String path = str_new();

        for (i = strlen(lexer->inp_file.file); (i - 1) > 0; --i) {
          if (lexer->inp_file.file[i] == '/') {
            j = i;
            break;
          }
        }

        for (i = 0; i <= j; ++i)
          path = str_append_char(path, lexer->inp_file.file[i]);

        path = str_append(path, STR_CSTR(incl_file.value));
        to_free_push(path.data);

        incl_lexer = cang_lexer_new(STR_CSTR(path));
        cang_free_token(incl_file);
      } else { incl_lexer = cang_lexer_new(STR_CSTR(incl_file.value)); to_free_push(incl_file.value.data); }

      incl_ast = cang_parse_file(incl_lexer);

      cang_merge_nodes(ast, incl_ast);

      cang_lexer_free(incl_lexer);
      cang_single_node_free(incl_ast);
    } else if (STR_CSTR_CMP(identifier.value, "const")) {
      String name;
      CangNode *value;

      name = cang_expect_token(lexer, TOK_ID).value;
      value = cang_parse_expr(lexer, false);

      if (cang_constants_find(name) != NULL)
        log_compiling_error(lexer->inp_file, "redefinition of preprocessor constant `"STRFmt"`.", STRArg(name));

      cang_constants_push(name, value);
    } else log_compiling_error(lexer->inp_file, "unknown preprocessor command `"STRFmt"`.", STRArg(identifier.value));

    cang_free_token(identifier);

    node = cang_parse_stmt(ast, lexer);
  } break;
  case TOK_PROC: {
    CangNode *proc_def, *proc_body;

    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_PROC_DEF, lexer->inp_file);
    proc_def = cang_parse_expr(lexer, false);
    proc_body = cang_parse_stmt(ast, lexer);

    cang_add_node_child(node, proc_def);
    cang_add_node_child(node, proc_body);
  } break;
  case TOK_RET: {
    CangToken has_expr;

    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_RETURN, lexer->inp_file);

    has_expr = cang_peek_token(lexer);

    if (has_expr.kind != TOK_EOL)
      cang_add_node_child(node, cang_parse_expr(lexer, false));

    cang_free_token(has_expr);
    cang_free_token(cang_expect_token(lexer, TOK_EOL));
  } break;
  case TOK_IF: {
    CangToken else_peek;

    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_IF, lexer->inp_file);

    cang_add_node_child(node, cang_parse_expr(lexer, false));
    cang_add_node_child(node, cang_parse_stmt(ast, lexer));
    
    else_peek = cang_peek_token(lexer);

    if (else_peek.kind == TOK_ELSE) {
      cang_free_token(cang_expect_token(lexer, else_peek.kind));
      cang_add_node_child(node, cang_parse_stmt(ast, lexer));
    } cang_free_token(else_peek);
  } break;
  case TOK_WHILE: {
    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_WHILE, lexer->inp_file);

    cang_add_node_child(node, cang_parse_expr(lexer, false));
    cang_add_node_child(node, cang_parse_stmt(ast, lexer));
  } break;
  case TOK_FOR: {
    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_FOR, lexer->inp_file);

    cang_add_node_child(node, cang_parse_expr(lexer, false));
    cang_free_token(cang_expect_token(lexer, TOK_EOL));
    cang_add_node_child(node, cang_parse_expr(lexer, false));
    cang_free_token(cang_expect_token(lexer, TOK_EOL));
    cang_add_node_child(node, cang_parse_expr(lexer, false));
    cang_add_node_child(node, cang_parse_stmt(ast, lexer));
  } break;
  case TOK_VAR: {
    CangNode *var_name, *var_type;
    cang_free_token(cang_expect_token(lexer, token.kind));

    var_name = cang_parse_expr(lexer, false);
    var_type = cang_parse_expr(lexer, false);
    node = cang_node_new(NODE_STMT_KIND_VAR_DEF, lexer->inp_file);

    cang_add_node_child(node, var_name);
    cang_add_node_child(node, var_type);

    cang_free_token(cang_expect_token(lexer, TOK_EOL));
  } break;
  case TOK_LCURLY: {
    cang_free_token(token);
    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_COMPOUND, lexer->inp_file);

    token = cang_peek_token(lexer);

    while (token.kind != TOK_RCURLY) {
      cang_add_node_child(node, cang_parse_stmt(ast, lexer));
      cang_free_token(token);
      token = cang_peek_token(lexer);
    }

    cang_free_token(cang_expect_token(lexer, TOK_RCURLY));

  } break;
  case TOK_BREAK: {
    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_BREAK, lexer->inp_file);

    cang_free_token(cang_expect_token(lexer, TOK_EOL));
  } break;
  case TOK_CONTINUE: {
    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_CONTINUE, lexer->inp_file);

    cang_free_token(cang_expect_token(lexer, TOK_EOL));
  } break;
  case TOK_EOL: {
    cang_free_token(cang_expect_token(lexer, token.kind));

    node = cang_node_new(NODE_STMT_KIND_NOP, lexer->inp_file);
  } break;
  case TOK_EOF: {
    node = NULL;
  } break;
  default:
    tmp = cang_parse_expr(lexer, false);

    if (tmp->kind != NODE_STMT_KIND_SET
        && tmp->kind != NODE_STMT_KIND_ADD_SET
        && tmp->kind != NODE_STMT_KIND_SUB_SET
        && tmp->kind != NODE_STMT_KIND_MUL_SET
        && tmp->kind != NODE_STMT_KIND_DIV_SET) {
      node = cang_node_new(NODE_STMT_KIND_EXPR, lexer->inp_file);
      cang_add_node_child(node, tmp);
    } else node = tmp;

    cang_free_token(cang_expect_token(lexer, TOK_EOL));
  }

  cang_free_token(token);

  return node;
}

// Returns the full AST for the file associated with the lexer.
CangNode *cang_parse_file(CangLexer *lexer)
{
  CangNode *ast = cang_node_new(NODE_KIND_START, lexer->inp_file);
  CangNode *node = cang_parse_stmt(ast, lexer);

  while (node != NULL) {
    cang_add_node_child(ast, node);
    node = cang_parse_stmt(ast, lexer);
  }

  return ast;
}

void cang_parse_program(Cang *cang)
{
  cang->ast = cang_parse_file(cang->lexer);
}
