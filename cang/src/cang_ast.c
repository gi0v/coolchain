#include <assert.h>

#include <cang_ast.h>

CangList *cang_list_new(size_t item_sz)
{
  CangList *list = calloc(1, sizeof(*list));

  list->item_sz = item_sz;
  list->items_count = 0;
  list->items = calloc(list->items_count + 1, item_sz);

  return list;
}

void cang_list_free(CangList *list)
{
  free(list->items);
  free(list);
}

void *cang_list_add(CangList *list, void *item)
{
  list->items = realloc(list->items, list->item_sz * (list->items_count + 1));
  list->items[list->items_count] = item;
  list->items_count++;

  return list->items[list->items_count - 1];
}

CangNode *cang_node_new(CangNodeKind kind, FileLocation location)
{
  CangNode *node = calloc(1, sizeof(*node));

  node->kind = kind;
  node->has_precedence = false;
  node->children = cang_list_new(sizeof(CangNode *));
  node->location = location;

  return node;
}

CangNode *cang_node_dup(CangNode *node, FileLocation location)
{
  size_t i;
  CangNode *dup = calloc(1, sizeof(*dup));

  dup->kind = node->kind;
  dup->location = location;
  dup->has_precedence = node->has_precedence;
  dup->children = cang_list_new(sizeof(CangNode *));

  if (STR_CSTR(node->value)) dup->value = str_from_static_cstr(STR_CSTR(node->value));

  for (i = 0; i < node->children->items_count; ++i)
    cang_add_node_child(dup, cang_node_dup(node->children->items[i], location));

  return dup;
}

void cang_single_node_free(CangNode *node)
{
  str_free(node->value);
  cang_list_free(node->children);
  free(node);
}

void cang_node_free(CangNode *node)
{
  size_t i;

  str_free(node->value);

  for (i = 0; i < node->children->items_count; ++i)
    cang_node_free(node->children->items[i]);

  cang_list_free(node->children);
  free(node);
}

CangNode *cang_add_node_child(CangNode *parent, CangNode *child)
{
  if (child != NULL) return cang_list_add(parent->children, child);
  else return NULL;
}

CangNode *cang_get_node_child(CangNode *node, size_t index)
{
  if (index > node->children->items_count)
    return NULL;

  return node->children->items[index];
}

void cang_merge_nodes(CangNode *dst, CangNode *src)
{
  size_t i;

  assert(dst->kind == src->kind);

  for (i = 0; i < src->children->items_count; ++i)
    cang_add_node_child(dst, cang_get_node_child(src, i));
}
