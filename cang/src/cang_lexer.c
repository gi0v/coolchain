#include <ctype.h>
#include <assert.h>

#include <strlib.h>
#include <cang_lexer.h>
#include <error_logger.h>

void cang_free_token(CangToken tok) { str_free(tok.value); }

CangTokenKind cang_get_token_kind_from_chars(CangLexer *lexer, char fs, char ss)
{
  (void) ss; // Temp

  if (isalpha(fs) || fs == '_')  return TOK_ID;
  else if (isdigit(fs))          return TOK_NUM_LIT;
  else {
    switch (fs) {
    case '+':  if (ss == '=') return TOK_ADD_EQ; else return TOK_ADD;
    case '-':  if (ss == '=') return TOK_SUB_EQ; else return TOK_SUB;
    case '*':  if (ss == '=') return TOK_MUL_EQ; else return TOK_ASTERISK;
    case '/':  if (ss == '=') return TOK_DIV_EQ; else return TOK_DIV;
    case '%':  if (ss == '=') return TOK_MOD_EQ; else return TOK_MOD;
    case ';':  return TOK_EOL;
    case '\0': return TOK_EOF;
    case '=': if (ss == '=') return TOK_EQU; else return TOK_SET;
    case '!': if (ss == '=') return TOK_NEQ; else assert(false && "TODO: Expression negation: Not implemented.");
    case '>': if (ss == '=') return TOK_GEQ; else if (ss == '>') return TOK_SHR; else return TOK_GRE;
    case '<': if (ss == '=') return TOK_LEQ; else if (ss == '<') return TOK_SHL; else return TOK_LES;
    case ':':  return TOK_TSET;
    case '|':  return TOK_PIPE;
    case '^':  return TOK_CFLEX;
    case ',':  return TOK_COMMA;
    case '{':  return TOK_LCURLY;
    case '}':  return TOK_RCURLY;
    case '(':  return TOK_LPAREN;
    case ')':  return TOK_RPAREN;
    case '[':  return TOK_LBRACKET;
    case ']':  return TOK_RBRACKET;
    case '&':  return TOK_AMPERSAND;
    case '\'': return TOK_CHR_LIT;
    case '"':  return TOK_STR_LIT;
    case '@':  return TOK_PREPROC;
    case '#':  return TOK_COMMENT;
    default:   log_compiling_error(lexer->inp_file, "encountered unknown token `%c`", fs); return TOK_UNKNOWN;
    }
  }
}

CangTokenKind cang_get_token_kind_from_id(String id)
{
  if (STR_CSTR_CMP(id, "proc")) return TOK_PROC;
  else if (STR_CSTR_CMP(id, "return")) return TOK_RET;
  else if (STR_CSTR_CMP(id, "if")) return TOK_IF;
  else if (STR_CSTR_CMP(id, "else")) return TOK_ELSE;
  else if (STR_CSTR_CMP(id, "var")) return TOK_VAR;
  else if (STR_CSTR_CMP(id, "for")) return TOK_FOR;
  else if (STR_CSTR_CMP(id, "while")) return TOK_WHILE;
  else if (STR_CSTR_CMP(id, "break")) return TOK_BREAK;
  else if (STR_CSTR_CMP(id, "continue")) return TOK_CONTINUE;
  else return TOK_ID;
}

CangToken cang_peek_id(CangLexer *lexer, size_t i)
{
  String str = str_new();
  CangToken tok;

  char next = str_get_char(lexer->src, i++);

  while (isalpha(next) || next == '_' || isdigit(next)) {
    str = str_append_char(str, next);
    next = str_get_char(lexer->src, i++);
  }

  tok.kind = cang_get_token_kind_from_id(str);
  tok.value = str;

  return tok;
}

CangToken cang_lex_id(CangLexer *lexer)
{
  String str = str_new();
  CangToken tok;

  char next = str_get_char(lexer->src, 0);

  while (isalpha(next) || next == '_' || isdigit(next)) {
    lexer->inp_file.col++;
    lexer->src = str_remove_char(lexer->src, 0);
    str = str_append_char(str, next);
    next = str_get_char(lexer->src, 0);
  }

  tok.kind = cang_get_token_kind_from_id(str);
  tok.value = str;

  return tok;
}

String cang_peek_num(CangLexer *lexer, size_t i)
{
  String ret = str_new();

  char next = str_get_char(lexer->src, i++);

  while (isdigit(next)) {
    ret = str_append_char(ret, next);
    next = str_get_char(lexer->src, i++);
  }

  return ret;
}

String cang_lex_num(CangLexer *lexer)
{
  String ret = str_new();

  char next = str_get_char(lexer->src, 0);

  while (isdigit(next)) {
    lexer->inp_file.col++;
    lexer->src = str_remove_char(lexer->src, 0);
    ret = str_append_char(ret, next);
    next = str_get_char(lexer->src, 0);
  }

  return ret;
}

String cang_peek_chr(CangLexer *lexer, size_t i)
{
  String ret = str_new();

  ret = str_append_char(ret, str_get_char(lexer->src, i + 1));

  return ret;
}

String cang_lex_chr(CangLexer *lexer)
{
  char c;
  String ret;

  str_free(str_chop_left(&lexer->src, 1));
  lexer->inp_file.col++;

  if (str_get_char(lexer->src, 0) == '\\') {
    ret = str_new();

    switch (str_get_char(lexer->src, 1)) {
    case 'a': ret = str_append_char(ret, '\a'); break;
    case 'b': ret = str_append_char(ret, '\b'); break;
    case 'n': ret = str_append_char(ret, '\n'); break;
    case 'r': ret = str_append_char(ret, '\r'); break;
    case 't': ret = str_append_char(ret, '\t'); break;
    case 'v': ret = str_append_char(ret, '\v'); break;
    case '\\': ret = str_append_char(ret, '\\'); break;
    case '\'': ret = str_append_char(ret, '\''); break;
    case '"': ret = str_append_char(ret, '"'); break;
    }

    str_free(str_chop_left(&lexer->src, 2));
    lexer->inp_file.col += 2;
  } else {
    ret = str_chop_left(&lexer->src, 1);
    lexer->inp_file.col++;
  }

  if ((c = str_get_char(lexer->src, 0)) != '\'')
    log_compiling_error(lexer->inp_file, "expected apostrophe after character literal, but got `%c`.", c);

  str_free(str_chop_left(&lexer->src, 1));
  lexer->inp_file.col++;

  return ret;
}

// TODO: There's no way to peek strings, however this is not strictly needed for now.
String cang_lex_str(CangLexer *lexer)
{
  char c;
  String ret;

  ret = str_new();

  lexer->inp_file.col++;
  str_free(str_chop_left(&lexer->src, 1));

  while (lexer->src.length) {
    c = str_get_char(lexer->src, 0);

    if (c == '\\') {
      c = str_get_char(lexer->src, 1);

      switch (c) {
      case 'a': ret = str_append_char(ret, '\a'); break;
      case 'b': ret = str_append_char(ret, '\b'); break;
      case 'n': ret = str_append_char(ret, '\n'); break;
      case 'r': ret = str_append_char(ret, '\r'); break;
      case 't': ret = str_append_char(ret, '\t'); break;
      case 'v': ret = str_append_char(ret, '\v'); break;
      case '\\': ret = str_append_char(ret, '\\'); break;
      case '\'': ret = str_append_char(ret, '\''); break;
      case '"': ret = str_append_char(ret, '"'); break;
      }

      lexer->inp_file.col += 2;
      lexer->src = str_remove_char(lexer->src, 0);
      lexer->src = str_remove_char(lexer->src, 0);
    } else if (c == '"') break;
    else { lexer->inp_file.col++; lexer->src = str_remove_char(lexer->src, 0); ret = str_append_char(ret, c); }
  }

  lexer->inp_file.col++; 
  str_free(str_chop_left(&lexer->src, 1));

  return ret;
}

CangToken cang_next_token(CangLexer *lexer)
{
  char fs, ss;
  CangToken tok = {0};

  fs = str_get_char(lexer->src, 0);
  
  while (fs == ' ' || fs == '\t' || fs == '\n') {
    str_remove_char(lexer->src, 0);

    if (fs == '\n') {
      lexer->inp_file.row++;
      lexer->inp_file.col = 1;
    } else lexer->inp_file.col++;

    fs = str_get_char(lexer->src, 0);
  }

  ss = str_get_char(lexer->src, 1);

  tok.kind = cang_get_token_kind_from_chars(lexer, fs, ss);

  switch (tok.kind) {
  case TOK_ID:      tok       = cang_lex_id(lexer);  break;
  case TOK_STR_LIT: tok.value = cang_lex_str(lexer); break;
  case TOK_CHR_LIT: tok.value = cang_lex_chr(lexer); break;
  case TOK_NUM_LIT: tok.value = cang_lex_num(lexer); break;
  case TOK_COMMENT:
    while (str_get_char(lexer->src, 0) != '\n') { str_remove_char(lexer->src, 0); lexer->inp_file.col++; }
    str_remove_char(lexer->src, 0);
    lexer->inp_file.row++;
    lexer->inp_file.col = 1;

    return cang_next_token(lexer);
  case TOK_EQU: case TOK_NEQ: case TOK_GRE: case TOK_LES: case TOK_GEQ: case TOK_LEQ: case TOK_SHL:
  case TOK_SHR: case TOK_ADD_EQ: case TOK_SUB_EQ: case TOK_MUL_EQ: case TOK_DIV_EQ:
    tok.value = str_chop_left(&lexer->src, 2); lexer->inp_file.col += 2;
    break;
  default: tok.value = str_chop_left(&lexer->src, 1); lexer->inp_file.col++;
  }

  return tok;
}

CangToken cang_expect_token(CangLexer *lexer, CangTokenKind expected_kind)
{
  CangToken tok = cang_next_token(lexer);

  if (tok.kind != expected_kind)
    log_compiling_error(lexer->inp_file,
                        "was expecting token kind %s, but got "STRFmt" instead.",
                        cang_token_kind_to_cstr(expected_kind), STRArg(cang_str_token(tok)));

  return tok;
}

CangToken cang_peek_token(CangLexer *lexer)
{
  char fs, ss;
  size_t i = 0;

 restart:
  fs = str_get_char(lexer->src, i++);

  while (fs == ' ' || fs == '\t' || fs == '\n')
    fs = str_get_char(lexer->src, i++);

  ss = str_get_char(lexer->src, i);

  CangTokenKind kind = cang_get_token_kind_from_chars(lexer, fs, ss);

  if (kind == TOK_ID) return cang_peek_id(lexer, i - 1);
  else if (kind == TOK_CHR_LIT) return (CangToken) { .kind = TOK_CHR_LIT, .value = cang_peek_chr(lexer, i - 1) };
  else if (kind == TOK_NUM_LIT) return (CangToken) { .kind = TOK_NUM_LIT, .value = cang_peek_num(lexer, i - 1) };
  else if (kind == TOK_COMMENT) {
    while (fs != '\n')
      fs = str_get_char(lexer->src, i++);

    goto restart;
  }
  else {
    CangToken tok;

    tok.kind = kind;
    tok.value = str_new();
    tok.value = str_append_char(tok.value, tok.kind);

    return tok;
  }
}

// TODO: Add the missing ones (for error reporting).
const char *cang_token_kind_to_cstr(CangTokenKind kind)
{
  switch (kind) {
  case TOK_ID: return "TOK_ID";
  case TOK_EOF: return "TOK_EOF";
  case TOK_NUM_LIT: return "TOK_NUM_LIT";
  case TOK_CHR_LIT: return "TOK_CHR_LIT";
  case TOK_STR_LIT: return "TOK_STR_LIT";
  case TOK_PROC: return "TOK_PROC";
  case TOK_RET: return "TOK_RET";
  case TOK_EQU: return "TOK_EQU";
  case TOK_NEQ: return "TOK_NEQ";
  case TOK_GRE: return "TOK_GRE";
  case TOK_LES: return "TOK_LES";
  case TOK_GEQ: return "TOK_GEQ";
  case TOK_LEQ: return "TOK_LEQ";
  case TOK_SHL: return "TOK_SHL";
  case TOK_SHR: return "TOK_SHR";
  case TOK_IF: return "TOK_IF";
  case TOK_ELSE: return "TOK_ELSE";
  case TOK_VAR: return "TOK_VAR";
  case TOK_FOR: return "TOK_FOR";
  case TOK_WHILE: return "TOK_WHILE";
  case TOK_ADD: return "TOK_ADD";
  case TOK_SUB: return "TOK_SUB";
  case TOK_ASTERISK: return "TOK_ASTERISK";
  case TOK_DIV: return "TOK_DIV";
  case TOK_MOD: return "TOK_MOD";
  case TOK_EOL: return "TOK_EOL";
  case TOK_SET: return "TOK_SET";
  case TOK_TSET: return "TOK_TSET";
  case TOK_PIPE: return "TOK_PIPE";
  case TOK_CFLEX: return "TOK_CFLEX";
  case TOK_COMMA: return "TOK_COMMA";
  case TOK_LCURLY: return "TOK_LCURLY";
  case TOK_RCURLY: return "TOK_RCURLY";
  case TOK_LPAREN: return "TOK_LPAREN";
  case TOK_RPAREN: return "TOK_RPAREN";
  case TOK_PREPROC: return "TOK_PREPROC";
  case TOK_LBRACKET: return "TOK_LBRACKET";
  case TOK_RBRACKET: return "TOK_RBRACKET";
  case TOK_AMPERSAND: return "TOK_AMPERSAND";
  case TOK_COMMENT: return "TOK_COMMENT";
  case TOK_UNKNOWN: return "TOK_UNKNOWN";
  case TOK_BREAK: return "TOK_BREAK";
  case TOK_CONTINUE: return "TOK_CONTINUE";
  case TOK_ADD_EQ: return "TOK_ADD_EQ";
  case TOK_SUB_EQ: return "TOK_SUB_EQ";
  case TOK_MUL_EQ: return "TOK_MUL_EQ";
  case TOK_DIV_EQ: return "TOK_DIV_EQ";
  case TOK_MOD_EQ: return "TOK_MOD_EQ";
  }

  return NULL;
}

String cang_str_token(CangToken tok)
{
  String ret = str_new();

  FMT_APPEND_TO_STR(ret, "%s (`"STRFmt"`)", cang_token_kind_to_cstr(tok.kind), STRArg(tok.value));

  return ret;
}

void cang_dump_token(CangToken tok, bool eol)
{
  printf("%s (`"STRFmt"`)", cang_token_kind_to_cstr(tok.kind), STRArg(tok.value));

  if (eol)
    printf("\n");
}
