import os, sys, subprocess
from colorama import Fore

# Source ext: .cang | Expected outputs ext: .cct (cool-chain-test)
test_files = [
    "./if_else",
    "./while",
    "./strings",
    "./local_arrays",
    "./global_arrays",
    "./recursion",
    "./pointers",
    "./alphabet",
    "./fib",
    "./fizzbuzz",
    "./rule110",
    "./for",
    "./const_macros",
]

def get_nasm_command(src): return f"../../toolchain/cang -nasm {src} -o {src[:-5]}.elf"
def get_run_command(src): return f"{src[:-5]}.elf"

def run_command(cmd, output=True):
    if output:
        print(f"[CMD] {cmd}")

    proc = subprocess.Popen(cmd.split(' '), stdout = subprocess.PIPE, stderr = subprocess.PIPE)

    stdout, stderr = proc.communicate()

    if output:
        print(stdout.decode("UTF-8"), end='')
        print(stderr.decode("UTF-8"), end='')

    return stdout.decode("UTF-8") + stderr.decode("UTF-8")

def record_outputs():
    test_dict = dict( [ (x + ".cang", x + ".cct") for x in test_files ] )

    for test_src, test_out in test_dict.items():
        with open(test_out, "w") as out:
            out.write(run_command(get_nasm_command(test_src)) + run_command(get_run_command(test_src)))

def run_tests():
    test_dict = dict( [ (x + ".cang", x + ".cct") for x in test_files ] )
    test_res = [0, 0]
    ##         (successes, fails)

    for test_src, test_out in test_dict.items():
        with open(test_out, "r") as out:
            print(f"Running test `{test_out[2:-4]}`... ", end='')
            command = run_command(get_nasm_command(test_src), False) + run_command(get_run_command(test_src), False)
            read = out.read()

            if read != command:
                test_res[1] += 1
                print(Fore.RED + "FAIL" + Fore.RESET)

                print("Got output:")
                print(command, end='')
                print("And was expecting:")
                print(read, end='')
            else:
                test_res[0] += 1
                print(Fore.GREEN + "OK" + Fore.RESET)

    print("\n-- RESULTS")
    print(f"--[ Succeded: {Fore.GREEN}{test_res[0]}{Fore.RESET}, Failed: {Fore.RED}{test_res[1]}{Fore.RESET} ]--")

    if test_res[1] > 0:
        exit(1)

def usage(pn):
    print(f"USAGE: {pn} [COMMAND]")
    print("COMMAND:")
    print("    record,  Records the expected outputs, run only when sure that everything is fine.")
    print("    run,     Runs the tests in the current directory")

    exit(1)

def main(argv):
    if len(argv) < 2:
        usage(argv[0])

    if argv[1] == "record":
        record_outputs()
    elif argv[1] == "run":
        run_tests()
    else: usage(argv[0])

if __name__ == "__main__": main(sys.argv)
