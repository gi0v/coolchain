(defvar cang-mode-syntax-table
  (let ((table (make-syntax-table)))
    ;; comments
    (modify-syntax-entry ?# "<" table)
    (modify-syntax-entry ?\n ">" table)
    ;; preprocessor
    (modify-syntax-entry ?@ "." table)
    ;; chars = strings
    (modify-syntax-entry ?' "\"" table)
    table))

(eval-and-compile
  (defconst cang-keywords '("if" "else" "while" "for" "var" "proc" "return" "cast" "break" "continue"))
  (defconst cang-types '("void" "uint8" "uint16" "uint32" "uint64"
			 "char" "void_ptr" "uint8_ptr" "uint16_ptr"
			 "uint32_ptr" "uint64_ptr" "char_ptr"))
  (defconst cang-functions '("load_ptr" "store_ptr" "main"))
  (defconst cang-events '("dump" "write" "abort" "getchar")))

(defconst cang-highlights
  `(
    (,(regexp-opt cang-keywords 'symbols) . font-lock-keyword-face)
    (,(regexp-opt cang-types 'symbols) . font-lock-type-face)
    (,(regexp-opt cang-functions 'symbols) . font-lock-function-name-face)
    (,(regexp-opt cang-events 'symbols) . font-lock-builtin-face)
    ("@ *[a-zA-Z0-9_]+" . font-lock-preprocessor-face)
    ))

(defun cang--space-prefix-len (line)
  (- (length line)
     (length (string-trim-left line))))

(defun cang--previous-non-empty-line ()
  (save-excursion
    (forward-line -1)
    (while (and (not (bobp))
                (string-empty-p
                 (string-trim-right
                  (thing-at-point 'line t))))
      (forward-line -1))
    (thing-at-point 'line t)))

(defun cang--desired-indentation ()
  (let ((cur-line (string-trim-right (thing-at-point 'line t)))
        (prev-line (string-trim-right (cang--previous-non-empty-line)))
        (indent-len 4))
    (cond
     ((and (string-suffix-p "{" prev-line)
           (string-prefix-p "}" (string-trim-left cur-line)))
      (cang--space-prefix-len prev-line))
     ((string-suffix-p "{" prev-line)
      (+ (cang--space-prefix-len prev-line) indent-len))
     ((string-prefix-p "}" (string-trim-left cur-line))
      (max (- (cang--space-prefix-len prev-line) indent-len) 0))
     (t (cang--space-prefix-len prev-line)))))

(defun cang-indent-line ()
  (interactive)
  (when (not (bobp))
    (let* ((current-indentation
            (cang--space-prefix-len (thing-at-point 'line t)))
           (desired-indentation
            (cang--desired-indentation))
           (n (max (- (current-column) current-indentation) 0)))
      (indent-line-to desired-indentation)
      (forward-char n))))

;;;###autoload
(define-derived-mode cang-mode prog-mode "cang"
  "Major Mode for editing Cang source code."
  :syntax-table cang-mode-syntax-table
  (setq font-lock-defaults '(cang-highlights))
  (setq tab-width 2)
  (setq-local indent-line-function 'cang-indent-line)
  (setq-local comment-start "# ")
  (setq-local comment-end ""))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.cang\\'" . cang-mode))

(provide 'cang-mode)
