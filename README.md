# Coolchain
### _The cool toolchain_

## What is this?

This is a compiling toolchain written in C that consists, as of now, of a virtual machine ([CM](cm/)), a debugger for the virtual machine bytecode ([cdb](cdb/)), an assembler for the virtual machine bytecode ([casm](casm/)), a disassembler for that bytecode ([decasm](decasm/)) and finally a compiler for a stack-based, procedural programming language that compiles down to CM bytecode through the assembler ([cang](cang/)).

## Features
- [x] [CM](cm/)
- - [x] Base instructions
- - [x] Native instructions (dump, write, abort)
- - [x] Turing complete (proved by Cang's [Rule 110](cang/examples/rule110.cang))
- [x] [Casm](casm/)
- - [x] Conversion from mnemonics to bytecode
- - [x] Labels
- - [x] Turing complete (proved by Cang's [Rule 110](cang/examples/rule110.cang))
- [x] [Decasm](decasm/)
- - [x] Capable of converting bytecode to Casm mnemonics.
- [x] [Cang](cang/)
- - [x] Procedural by design
- - [x] Stack-based
- - [x] Statically typed
- - [x] Variable scopes
- - [x] Turing complete (see [Rule 110](cang/examples/rule110.cang))
- - [x] Has typed arrays and typed pointers
- - [x] Preprocessed
- - - [x] Include directive
- - - [ ] Define directive
- [x] [Cdb](cdb/)
- - [x] Basic debugging features (single-step, continue, run, breakpoints)
- - [x] Debug traces (for now only visible on breakpoints and single-steps)

## Dependencies

To compile all of this you _can_ use [RCake](https://www.gitlab.com/gi0v1/rcake.git), my compiling system, right now it is included in the repository as a submodule.

## Getting started

### Setup

To use this toolchain you'll need to bootstrap the [build system](https://www.gitlab.com/gi0v1/rcake.git) first (you can also run all the commands yourself, but that's pretty inconvenient), you can find info on how to bootstrap it [here](https://gitlab.com/gi0v1/rcake/-/blob/main/README.md#bootstrapping), but for the base setup use you'll need to run these commands:
```bash
$ git clone https://gitlab.com/gi0v1/rcake.git
$ cd rcake
$ mkdir -p src && cd src
$ rustc -o ../dst/rcake rcake.rs
```

After that, you'll need add the directory to the `rcake` binary in your PATH:
```bash
$ export PATH=/path/to/rcake/dst/
```

Now, while in the root of the project, you can compile coolchain like this:

```bash
$ rcake all clean
```

The output directory with all the binaries will be `toolchain/`.

You can run some tests like this (They are only for Cang right now, however they provide as tests for Casm and CM and the Nasm transpiler too, because all of these are used by Cang):

```bash
$ rcake tests
```
