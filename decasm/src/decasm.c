#include <stdlib.h>
#include <common.h>
#include <decasm.h>
#include <cm_base.h>
#include <error_logger.h>

DeCasm *decasm_read_file(const char *fn)
{
  FILE *fp;
  DeCasm *ret;
  CmWord *program;
  size_t program_sz;

  fp = fopen(fn, "rb");

  if (fp == NULL)
    log_io_error("could not open input file: %s", fn);

  fseek(fp, 0, SEEK_END);
  program_sz = ftell(fp);
  rewind(fp);

  program = calloc(program_sz, sizeof(CmWord));

  fread(program, program_sz, sizeof(CmWord), fp);

  fclose(fp);

  ret = calloc(1, sizeof(*ret));
  ret->program = program;
  ret->program_sz = program_sz / sizeof(CmWord);

  return ret;
}

void decasm_disassemble_program(DeCasm *decasm, FILE * restrict stream)
{
  CmWord i;
  const char template[] = "    0x%016lX:\t\t%lX %lX\t\t%s";
  const char template_append[] = "\t%lX";

  for (i = 0; i < decasm->program_sz; i += 2) {
    fprintf(stream, template, i,
	    decasm->program[i], decasm->program[i + 1],
	    get_cm_instr_from_cm_instr_type(decasm->program[i]).name);

    if (get_cm_instr_from_cm_instr_type(decasm->program[i]).has_param)
      fprintf(stream, template_append, decasm->program[i + 1]);

    fprintf(stream, "\n");
  }
}
