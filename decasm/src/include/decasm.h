#ifndef _DECASM_H
#define _DECASM_H

#include <stdio.h>
#include <stddef.h>

typedef struct {
  CmWord *program;
  size_t program_sz;
} DeCasm;

DeCasm *decasm_read_file(const char *fn);
void decasm_disassemble_program(DeCasm *decasm, FILE * restrict stream);

#endif  // _DECASM_H
