#include <stdio.h>
#include <stdlib.h>

#define STRLIB_IMPLEMENTATION
#include <strlib.h>
#undef STRLIB_IMPLEMENTATION

#include <cdb.h>
#include <common.h>
#include <cm_base.h>
#include <error_logger.h>

void usage(const char * const program, FILE * restrict stream, int exit_code)
{
  fprintf(stream, "USAGE: %s [OPTIONS] [INPUT].cm\n", program);
  fprintf(stream, "OPTIONS:\n");
  fprintf(stream, "    -h,    Displays this message.\n");

  exit(exit_code);
}

int main(int argc, char **argv)
{
  Cm *cm;
  Cdb *cdb;
  const char *input_file = NULL;
  const char * const program_name = shift(&argc, &argv);

  if (argc < 1) usage(program_name, stderr, 1);

  while (argc) {
    char *val = shift(&argc, &argv);

    if (val[0] == '-') {
      switch (val[1]) {
      case 'h':	usage(program_name, stdout, 0);	break;
      }
    } else input_file = val;
  }

  if (input_file == NULL)
    log_args_error("input file was not specified.");

  cm = cm_new();
  cdb = cdb_init(cm);
  cm_load_program_from_file(cm, input_file);

  cdb_loop(cdb);

  cdb_free(cdb);

  return 0;
}
