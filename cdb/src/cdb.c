#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#include <cdb.h>
#include <strlib.h>
#include <cm_base.h>

Cdb *cdb_init(Cm *cm)
{
  Cdb *cdb = calloc(1, sizeof(*cdb));

  cdb->cm = cm;
  cdb->cm->ip = -1;
  cdb->breakpoints_count = 0;
  cdb->breakpoints = calloc(1, sizeof(CdbBreakPoint));

  return cdb;
}

void cdb_free(Cdb *cdb)
{
  free(cdb->breakpoints);
  cm_free(cdb->cm);
  free(cdb);
}

void cdb_push_breakpoint(Cdb *cdb, CmAddr addr)
{
  cdb->breakpoints_count++;
  cdb->breakpoints = realloc(cdb->breakpoints, (cdb->breakpoints_count + 1) * sizeof(CdbBreakPoint));
  cdb->breakpoints[cdb->breakpoints_count - 1].addr = addr;

  if (addr == 0) {
    cdb->breakpoints[cdb->breakpoints_count - 1].nbh_bp_addr = 0;
    cdb->breakpoints[cdb->breakpoints_count - 1].neighbourhood[0] = cdb->cm->program[0];
    cdb->breakpoints[cdb->breakpoints_count - 1].neighbourhood[1] = cdb->cm->program[1];
    cdb->breakpoints[cdb->breakpoints_count - 1].neighbourhood[2] = cdb->cm->program[2];
  } else {
    cdb->breakpoints[cdb->breakpoints_count - 1].nbh_bp_addr = 1;
    cdb->breakpoints[cdb->breakpoints_count - 1].neighbourhood[0] = cdb->cm->program[(addr / 2) - 1];
    cdb->breakpoints[cdb->breakpoints_count - 1].neighbourhood[1] = cdb->cm->program[(addr / 2) + 0];
    cdb->breakpoints[cdb->breakpoints_count - 1].neighbourhood[2] = cdb->cm->program[(addr / 2) + 1];
  }
}

void cdb_mem_trace(Cdb *cdb, CmAddr start, CmAddr end)
{
  size_t i, lns_count;

  printf("--- Memory -----------------------------------------------------------------------------------------------\n");
  printf("0x%016X:\t", 0);

  lns_count = 0;

  for (i = start; i < end; ++i, lns_count++) {
    if (lns_count == 15) {
      lns_count = 0;
      printf("\n0x%016lX:\t", i);
    }

    printf("0x%02X ", cdb->cm->mem[i]);
  }

  printf("\n");
}

void cdb_dump_instr(CmInstr instr, CmAddr addr, bool has_bp)
{
  if (has_bp) printf("(!)");
  else printf("   ");

  printf("  0x%016lX:\t%s", addr, instr.name);
  
  if (instr.has_param)
    printf("  %lu", instr.parameter);

  printf("\n");
}

void cdb_full_trace(Cdb *cdb)
{
  CmAddr i;
  size_t lns_count;

  lns_count = 0;

  printf("--- Code -------------------------------------------------------------------------------------------------\n");
  cdb_dump_instr(cdb->cm->program[cdb->cm->ip - 1], (cdb->cm->ip - 1) * 2, false);
  cdb_dump_instr(cdb->cm->program[cdb->cm->ip], (cdb->cm->ip) * 2, false);
  cdb_dump_instr(cdb->cm->program[cdb->cm->ip + 1], (cdb->cm->ip + 1) * 2, false);
  printf("--- Stack ------------------------------------------------------------------------------------------------\n");
  printf("0x%016X:\t", 0);

  for (i = 0; i < cdb->cm->sp; ++i, lns_count++) {
    if (lns_count == 4) {
      lns_count = 0;
      printf("\n0x%016lX:\t", i);
    }

    printf("0x%016lX ", cdb->cm->stack[i]);
  }

  printf("\n");
  cdb_mem_trace(cdb, 0, 105);
  printf("--- Registers --------------------------------------------------------------------------------------------\n");
  printf("IP: 0x%016lX\n", cdb->cm->ip);
  printf("SP: 0x%016lX\n", cdb->cm->sp);
  printf("----------------------------------------------------------------------------------------------------------\n");
}

void cdb_proceed(Cdb *cdb)
{
  size_t j;

  if (cdb->cm->halt) {
    printf("ERROR: Program is not running.\n");
    return;
  }

  while (!cdb->cm->halt) {
    for (j = 0; j < cdb->breakpoints_count; ++j) {
      if (((cdb->cm->ip + 1) * 2) == cdb->breakpoints[j].addr) {
        printf("(!): Breakpoint hit.\n");
        cdb_full_trace(cdb);
        goto breakpoint;
      }
    }

    cdb->cm->ip++;
    cdb->cm->program[cdb->cm->ip].execute(cdb->cm, cdb->cm->program[cdb->cm->ip].parameter);
    continue;
    
  breakpoint:
    break;
  }
}

void cdb_interpret_cmd(Cdb *cdb, String cmd)
{
  size_t i = 0;

  if (str_find_char_right(cmd, '\n', &i))
    cmd = str_remove_char(cmd, i);

  if (str_find_char_left(cmd, ' ', &i)) {
    StringArray cmd_split;

    cmd_split = str_split(cmd, ' ');
    cmd_split.strings[cmd_split.count - 1].length--;

    if (STR_CSTR_CMP(cmd_split.strings[0], "break")) {
      CmAddr addr = strtoull(STR_CSTR(cmd_split.strings[1]), NULL, 16);

      if (addr % 2 != 0) printf("ERROR: Breakpoint address needs to be an even number.\n");
      else cdb_push_breakpoint(cdb, addr);
    } else if (STR_CSTR_CMP(cmd_split.strings[0], "bp")) {
      size_t k = strtoul(STR_CSTR(cmd_split.strings[1]), NULL, 16);

      if (k >= cdb->breakpoints_count) printf("ERROR: Breakpoint #%zu does not exist.\n", i);
      else {
        if (cdb->breakpoints[k].nbh_bp_addr == 0) {
          cdb_dump_instr(cdb->breakpoints[k].neighbourhood[0], cdb->breakpoints[k].addr, true);
          cdb_dump_instr(cdb->breakpoints[k].neighbourhood[1], cdb->breakpoints[k].addr + 2, false);
          cdb_dump_instr(cdb->breakpoints[k].neighbourhood[2], cdb->breakpoints[k].addr + 4, false);
        } else {
          cdb_dump_instr(cdb->breakpoints[k].neighbourhood[0], cdb->breakpoints[k].addr - 2, false);
          cdb_dump_instr(cdb->breakpoints[k].neighbourhood[1], cdb->breakpoints[k].addr + 0, true);
          cdb_dump_instr(cdb->breakpoints[k].neighbourhood[2], cdb->breakpoints[k].addr + 2, false);
        }
      }
    } else if (STR_CSTR_CMP(cmd_split.strings[0], "decasm")) {
      CmAddr addr1 = strtoull(STR_CSTR(cmd_split.strings[1]), NULL, 16);
      CmAddr addr2 = strtoull(STR_CSTR(cmd_split.strings[2]), NULL, 16);
      CmAddr j;

      for (j = addr1; j <= addr2; j += 0x2) {
        size_t k;
        bool has_bp = false;
        if (j == (cdb->cm->ip + 1) * 2) printf("==>  ");
        else printf("     ");

        for (k = 0; k < cdb->breakpoints_count; ++k)
          if (cdb->breakpoints[k].addr == j)
            has_bp = true;

        cdb_dump_instr(cdb->cm->program[(j / 2)], j, has_bp);
      }
    } else if (STR_CSTR_CMP(cmd_split.strings[0], "mem")) {
      CmAddr addr1 = strtoull(STR_CSTR(cmd_split.strings[1]), NULL, 16);
      CmAddr addr2 = strtoull(STR_CSTR(cmd_split.strings[2]), NULL, 16);
      
      cdb_mem_trace(cdb, addr1, addr2);
    } else printf("ERROR: Unknown command: "STRFmt".\n", STRArg(cmd_split.strings[0]));

    str_arr_free(cmd_split);
  } else {
    if (STR_CSTR_CMP(cmd, "exit")) {
      str_free(cmd);
      cdb_free(cdb);
      exit(0);
    } else if (STR_CSTR_CMP(cmd, "bl")) {
      size_t j;

      for (j = 0; j < cdb->breakpoints_count; ++j)
        printf("Breakpoint #%zu at address %lu.\n", j, cdb->breakpoints[j].addr);
    } else if (STR_CSTR_CMP(cmd, "si")) {
      if (cdb->cm->halt)
        printf("ERROR: Program is not running.\n");
      else {
        cdb->cm->ip++;
        cdb->cm->program[cdb->cm->ip].execute(cdb->cm, cdb->cm->program[cdb->cm->ip].parameter);
        printf("(S): Single step.\n");
        cdb_full_trace(cdb);
      }
    } else if (STR_CSTR_CMP(cmd, "c")) {
      cdb_proceed(cdb);
    } else if (STR_CSTR_CMP(cmd, "r")) {
      cdb->cm->ip = -1;
      cdb->cm->sp = 0;
      cdb->cm->halt = false;
      memset(cdb->cm->mem, 0, MEM_CAP * sizeof(CmByte));
      memset(cdb->cm->stack, 0, STACK_CAP * sizeof(CmWord));

      cdb_proceed(cdb);
    } else printf("ERROR: Unknown command: "STRFmt".\n", STRArg(cmd));
  }
}

void cdb_loop(Cdb *cdb)
{
  char cmd[1024];
  String cmd_str;

  while (true) {
    printf("cdb> ");
    fgets(cmd, sizeof cmd, stdin);

    cmd_str = str_from_static_cstr(cmd);
    cdb_interpret_cmd(cdb, cmd_str);
    str_free(cmd_str);

    memset(cmd, 0, sizeof cmd);
  }
}
