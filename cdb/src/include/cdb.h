#ifndef _CDB_H
#define _CDB_H

#include <strlib.h>
#include <cm_base.h>

typedef struct {
  CmAddr addr;
  size_t nbh_bp_addr;
  CmInstr neighbourhood[3];
} CdbBreakPoint;

typedef struct {
  Cm *cm;
  size_t breakpoints_count;
  CdbBreakPoint *breakpoints;
} Cdb;

Cdb *cdb_init(Cm *cm);
void cdb_free(Cdb *cdb);
void cdb_push_breakpoint(Cdb *cdb, CmAddr addr);
void cdb_interpret_cmd(Cdb *cdb, String cmd);
void cdb_loop(Cdb *cdb);

#endif  // _CDB_H
