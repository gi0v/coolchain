#ifndef _CASM_H
#define _CASM_H

#include <common.h>
#include <strlib.h>

typedef struct {
  String label_name;
  CmWord address;
} LabelItem;

typedef struct {
  LabelItem *items;
  size_t items_count;
} LabelsTable;

typedef struct {
  size_t bc_sz;
  char *input_file;
  CmWord *bytecode;
  size_t current_ln;
  CmAddr current_addr;
  LabelsTable *labels_table;
  StringArray src_split;
} Casm;

Casm *casm_new(const char *file_name);
Casm *casm_dynamic_new();
void casm_free(Casm *casm);
LabelsTable *labels_table_new();
void labels_table_add(LabelsTable *table, String name, CmWord addr);
CmWord labels_table_search(LabelsTable *table, String name);
void labels_table_print(LabelsTable *table);

#endif  // _CASM_H
