#ifndef _CASM_TRANSPILER_H
#define _CASM_TRANSPILER_H

#include <casm.h>
#include <nasm_wrap.h>

void casm_transpile_program(Casm *casm, Nasm *nasm, const char *output_file);
void casm_transpile_bootstrap_asm(Nasm *nasm);

#endif  // _CASM_TRANSPILER_H
