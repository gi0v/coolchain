#ifndef _CASM_OPS_H
#define _CASM_OPS_H

#include <stdlib.h>

#include <casm.h>
#include <common.h>
#include <cm_base.h>

CmWord *casm_bytecode_new(Casm *casm);
void casm_push_bytecode(Casm *casm, CmWord val);
void casm_push_instr(Casm *casm, CmInstrType instr_type, CmWord param);
void casm_set_place_holder(Casm *casm, CmWord place_holder, CmWord value);
void casm_assemble_source(Casm *casm);
void casm_write_program_to_file(Casm *casm, const char *of);

#endif  // _CASM_OPS_H
