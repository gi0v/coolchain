#ifndef _CASM_LABELS_H
#define _CASM_LABELS_H

#include <casm.h>

void casm_push_label(Casm *casm, String name);
CmWord casm_get_label_addr(Casm *casm, String name);
void casm_label_search(Casm *casm);

#endif  // _CASM_LABELS_H
