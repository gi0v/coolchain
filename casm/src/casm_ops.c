#include <stdio.h>

#include <common.h>
#include <casm_ops.h>
#include <casm_labels.h>
#include <error_logger.h>

CmWord *casm_bytecode_new(Casm *casm)
{
  CmWord *bytecode;

  bytecode = calloc(1, sizeof(CmWord));
  casm->current_addr = 0;

  return bytecode;
}

// This is the primitive on which all the casm_push_*() procedures rely on (except casm_push_label()).
void casm_push_bytecode(Casm *casm, CmWord val)
{
  casm->bytecode = realloc(casm->bytecode,
                           sizeof(CmWord)
                           * casm->bc_sz++);

  casm->bytecode[casm->bc_sz - 2] = val;
  casm->current_addr++;
}

void casm_push_instr(Casm *casm, CmInstrType instr_type, CmWord param)
{
  casm_push_bytecode(casm, (CmWord) instr_type);
  casm_push_bytecode(casm, param);
}

void casm_set_place_holder(Casm *casm, CmWord place_holder, CmWord value)
{
  size_t i;

  for (i = 0; i < casm->bc_sz - 1; ++i)
    if (casm->bytecode[i] == place_holder)
      casm->bytecode[i] = value;
}

void casm_assemble_source(Casm *casm)
{
  size_t i;

  for (i = 0; i < casm->src_split.count; ++i) {
    String str = casm->src_split.strings[i];

    if (str_get_char(str, 0) != '%' && str.length > 1) {
      StringArray ln_split = str_split(str, ' ');

      if (ln_split.count > 1) {
        if (STR_CSTR(ln_split.strings[1])[0] == '%') {
          ln_split.strings[1] = str_remove_char(ln_split.strings[1], '%');

          CmWord addr = casm_get_label_addr(casm, ln_split.strings[1]);

          if (addr == MAX_CM_WORD)
            log_assembling_error("%s:%zu: label not found: %s", casm->input_file, casm->current_ln, STR_CSTR(ln_split.strings[1]));

          casm_push_instr(casm, get_cm_instr_type_from_str(STR_CSTR(ln_split.strings[0])), addr);
        } else {
          casm_push_instr(casm, get_cm_instr_type_from_str(STR_CSTR(ln_split.strings[0])), strtoull(STR_CSTR(ln_split.strings[1]), NULL, 10));
        }
      } else {
        casm_push_instr(casm, get_cm_instr_type_from_str(STR_CSTR(ln_split.strings[0])), 0);
      }

      str_arr_free(ln_split);
    }

    casm->current_ln++;
  }
}

void casm_write_program_to_file(Casm *casm, const char *of)
{
  FILE *fp;
  size_t i;

  fp = fopen(of, "wb");

  if (fp == NULL)
    log_io_error("could not open output file: %s", of);

  for (i = 0; i < casm->bc_sz - 1; ++i)
    fwrite(&casm->bytecode[i], 1, sizeof(CmWord), fp);

  fclose(fp);
}
