#include <stdio.h>
#include <stdlib.h>

#include <casm.h>
#include <strlib.h>
#include <casm_ops.h>
#include <error_logger.h>

Casm *casm_new(const char *file_name)
{
  Casm *casm;
  size_t i;
  String src_str;

  casm = calloc(1, sizeof(Casm));

  if (!str_read_file(STR((char *)file_name), &src_str))
    log_io_error("there was an error opening / reading from file: `%s`", file_name);

  while (str_find_char_left(src_str, '\t', &i))
    src_str = str_remove_char(src_str, i);

  casm->input_file = (char *) file_name;
  casm->current_ln = 1;
  casm->labels_table = labels_table_new();
  casm->src_split = str_split(src_str, '\n');
  casm->bytecode = casm_bytecode_new(casm);
  casm->bc_sz = 1;

  str_free(src_str);

  return casm;
}

Casm *casm_dynamic_new()
{
  Casm *casm;

  casm = calloc(1, sizeof(Casm));

  casm->input_file = NULL;
  casm->current_ln = 1;
  casm->labels_table = labels_table_new();
  casm->bytecode = casm_bytecode_new(casm);
  casm->bc_sz = 1;

  return casm;
}

void casm_free(Casm *casm)
{
  size_t i;

  free(casm->bytecode);

  for (i = 0; i < casm->labels_table->items_count - 1; ++i) {
    str_free(casm->labels_table->items[i].label_name);
  }

  free(casm->labels_table->items);
  free(casm->labels_table);
  str_arr_free(casm->src_split);
  free(casm);
}

LabelsTable *labels_table_new()
{
  LabelsTable *table;

  table = calloc(1, sizeof(*table));

  table->items_count = 1;
  table->items = calloc(table->items_count, sizeof(LabelItem));

  return table;
}

void labels_table_add(LabelsTable *table, String name, CmWord addr)
{
  table->items = realloc(table->items,
			 (table->items_count + 1)
			 * sizeof(LabelItem));

  table->items[table->items_count - 1].label_name = str_from_static_cstr(STR_CSTR(name));
  table->items[table->items_count - 1].address = addr;

  table->items_count++; 
}

CmWord labels_table_search(LabelsTable *table, String name)
{
  size_t i;

  for (i = 0; i < table->items_count - 1; ++i)
    if (STR_CMP(table->items[i].label_name, name))
      return table->items[i].address;

  return MAX_CM_WORD;
}

void labels_table_print(LabelsTable *table)
{
  size_t i;

  printf("---------------------------------------------\n");

  for (i = 0; i < table->items_count - 1; ++i) {
    printf("Label: "STRFmt, STRArg(table->items[i].label_name));
    printf("Address: 0x%016lX\n", table->items[i].address);
    printf("---------------------------------------------\n");
  }
}
