#include <casm.h>
#include <casm_comments.h>

void casm_remove_comments(Casm *casm)
{
  size_t i, j;

  for (i = 0; i < casm->src_split.count; ++i) {
    if (str_find_char_left(casm->src_split.strings[i], ';', &j))
      str_free(str_chop_right(&casm->src_split.strings[i], casm->src_split.strings[i].length - j - 1));
  }
}
