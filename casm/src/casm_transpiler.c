/**
   Important information
   =====================

   - Labels:
     - addr_X: Each instruction will have it's own label,
               so that you can jmp X and go to the exact
               instruction you like.
     - _start: Program entry.
   - Registers:
     - %r10 -> %r14: Temporary registers for stack operations.
     - %r15: Register for base stack.

**/

#include <assert.h>

#include <cm_base.h>
#include <nasm_wrap.h>
#include <casm_transpiler.h>

#define PUSH_ADDR_LABEL(nasm, n)                          \
  do {                                                    \
    String addr_tmp = str_new();                          \
    addr_tmp = str_append(addr_tmp, "addr_");             \
    FMT_APPEND_TO_STR(addr_tmp, "%lu", n);                \
    nasm_push_label(nasm, STR_CSTR(addr_tmp));            \
    str_free(addr_tmp);                                   \
  } while (false)                                         \
    
#define WRITE_INSTR(nasm, reg, sz)                              \
  do {                                                          \
    PUSH_ADDR_LABEL(nasm, i);                                   \
    nasm_push_instr(nasm, 1, "pop", "rax");                     \
    nasm_push_instr(nasm, 1, "pop", "r10");                     \
    nasm_push_instr(nasm, 2, "mov "sz, "[r15 + r10]", reg);     \
  } while(false)                                                \
    
#define READ_INSTR(nasm, reg, sz, ext)                          \
  do {                                                          \
    PUSH_ADDR_LABEL(nasm, i);                                   \
    nasm_push_instr(nasm, 1, "pop", "r10");                     \
    nasm_push_instr(nasm, 2, "mov", reg, sz" [r15 + r10]");     \
    nasm_push_instr(nasm, 0, ext);                              \
    nasm_push_instr(nasm, 1, "push", "rax");                    \
  } while(false)                                                \

void casm_transpile_program(Casm *casm, Nasm *nasm, const char *output_file)
{
  size_t i;

  casm_transpile_bootstrap_asm(nasm);

  for (i = 0; i < casm->bc_sz - 1; i += 2) {
    switch (casm->bytecode[i]) {
    case INSTR_PUSH: {
      if (casm->bytecode[i + 2] != INSTR_JMP
          && casm->bytecode[i + 2] != INSTR_CJMP
          && casm->bytecode[i + 2] != INSTR_CALL) {
        PUSH_ADDR_LABEL(nasm, i);
        String tmp_label = str_new();
        FMT_APPEND_TO_STR(tmp_label, "%lu", casm->bytecode[i + 1]);
        nasm_push_instr(nasm, 1, "push", STR_CSTR(tmp_label));
        str_free(tmp_label);
      }
    } break;
    case INSTR_ADD: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "add", "r10", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_SUB: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "sub", "r10", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_MUL: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "imul", "r10", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_DIV: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "rbx");
      nasm_push_instr(nasm, 1, "pop", "rax");
      nasm_push_instr(nasm, 1, "idiv", "rbx");
      nasm_push_instr(nasm, 1, "push", "rax");
    } break;
    case INSTR_MOD: {
      PUSH_ADDR_LABEL(nasm, i);
      // This is a special case where we won't use r10 -> r15 regs.
      nasm_push_instr(nasm, 2, "xor", "rdx", "rdx");
      nasm_push_instr(nasm, 1, "pop", "rbx");
      nasm_push_instr(nasm, 1, "pop", "rax");
      nasm_push_instr(nasm, 1, "div", "rbx");
      nasm_push_instr(nasm, 1, "push", "rdx");
    } break;
    case INSTR_EQU: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "r12", "0");
      nasm_push_instr(nasm, 2, "mov", "r13", "1");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "r11");
      nasm_push_instr(nasm, 2, "cmove", "r12", "r13");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_NEQ: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "r12", "0");
      nasm_push_instr(nasm, 2, "mov", "r13", "1");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "r11");
      nasm_push_instr(nasm, 2, "cmovne", "r12", "r13");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_GRE: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "r12", "0");
      nasm_push_instr(nasm, 2, "mov", "r13", "1");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "r11");
      nasm_push_instr(nasm, 2, "cmovg", "r12", "r13");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_LES: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "r12", "0");
      nasm_push_instr(nasm, 2, "mov", "r13", "1");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "r11");
      nasm_push_instr(nasm, 2, "cmovl", "r12", "r13");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_GEQ: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "r12", "0");
      nasm_push_instr(nasm, 2, "mov", "r13", "1");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "r11");
      nasm_push_instr(nasm, 2, "cmovge", "r12", "r13");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_LEQ: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "r12", "0");
      nasm_push_instr(nasm, 2, "mov", "r13", "1");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "r11");
      nasm_push_instr(nasm, 2, "cmovle", "r12", "r13");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_JMP: {
      PUSH_ADDR_LABEL(nasm, i - 2);

      // Here we are supposing that the instruction before this is a
      // push of the address, without any stack manipulation.

      String tmp_label = str_new();
      tmp_label = str_append(tmp_label, "addr_");
      FMT_APPEND_TO_STR(tmp_label, "%lu", casm->bytecode[i - 1]);
      nasm_push_instr(nasm, 1, "jmp", STR_CSTR(tmp_label));
      str_free(tmp_label);
    } break;
    case INSTR_CJMP: {
      PUSH_ADDR_LABEL(nasm, i - 2);

      // Here we are supposing that the instruction before this is a
      // push of the address, without any stack manipulation.

      String tmp_label = str_new();
      tmp_label = str_append(tmp_label, "addr_");
      FMT_APPEND_TO_STR(tmp_label, "%lu", casm->bytecode[i - 1]);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "1");
      nasm_push_instr(nasm, 1, "jge", STR_CSTR(tmp_label));
      str_free(tmp_label);
    } break;
    case INSTR_DUP: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 1, "push", "r10");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_SWAP: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
      nasm_push_instr(nasm, 1, "push", "r11");
    } break;
    case INSTR_DROP: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "xor", "r10", "r10");
    } break;
    case INSTR_OVER: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "push", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
      nasm_push_instr(nasm, 1, "push", "r11");
    } break;
    case INSTR_ROT: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r12");
      nasm_push_instr(nasm, 1, "push", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
      nasm_push_instr(nasm, 1, "push", "r12");
    } break;
    case INSTR_NOP: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 0, "nop");
    } break;
    case INSTR_CALL: {
      PUSH_ADDR_LABEL(nasm, i - 2);

      // Here we are supposing that the instruction before this is a
      // push of the address, without any stack manipulation.

      String tmp_label = str_new();
      tmp_label = str_append(tmp_label, "addr_");
      FMT_APPEND_TO_STR(tmp_label, "%lu", casm->bytecode[i - 1]);
      nasm_push_instr(nasm, 1, "call", STR_CSTR(tmp_label));
      str_free(tmp_label);
    } break;
    case INSTR_RET: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 0, "ret");
    } break;
    case INSTR_WR8: {
      WRITE_INSTR(nasm, "al", "BYTE");
    } break;
    case INSTR_WR16: {
      WRITE_INSTR(nasm, "ax", "WORD");
    } break;
    case INSTR_WR32: {
      WRITE_INSTR(nasm, "eax", "DWORD");
    } break;
    case INSTR_WR64: {
      WRITE_INSTR(nasm, "rax", "QWORD");
    } break;
    case INSTR_RD8: {
      READ_INSTR(nasm, "al", "BYTE", "movsx rax, al");
    } break;
    case INSTR_RD16: {
      READ_INSTR(nasm, "ax", "WORD", "movsx rax, ax");
    } break;
    case INSTR_RD32: {
      READ_INSTR(nasm, "eax", "DWORD", "movsx rax, eax");
    } break;
    case INSTR_RD64: {
      READ_INSTR(nasm, "rax", "QWORD", "nop");
    } break;
    case INSTR_NOT: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "cmp", "r10", "0");
      nasm_push_instr(nasm, 1, "sete", "al");
      nasm_push_instr(nasm, 2, "movzx", "rax", "al");
      nasm_push_instr(nasm, 1, "push", "rax");
    } break;
    case INSTR_NAT: {
      PUSH_ADDR_LABEL(nasm, i);
      switch (casm->bytecode[i + 1]) {
      case NATIVE_DUMP:
        nasm_push_instr(nasm, 1, "pop", "rdi");
        nasm_push_instr(nasm, 1, "call", "dump");
        break;
      case NATIVE_WRITE:
        nasm_push_instr(nasm, 1, "pop", "r10");
        nasm_push_instr(nasm, 2, "add", "r10", "r15");
        nasm_push_instr(nasm, 1, "call", "strlen");
        nasm_push_instr(nasm, 2, "mov", "rax", "1");
        nasm_push_instr(nasm, 2, "mov", "rdi", "1");
        nasm_push_instr(nasm, 2, "mov", "rsi", "r10");
        nasm_push_instr(nasm, 2, "mov", "rdx", "r11");
        nasm_push_instr(nasm, 0, "syscall");
        break;
      case NATIVE_READ:
        nasm_push_instr(nasm, 1, "call", "getchar");
        nasm_push_instr(nasm, 1, "push", "rax");
        break;
      case NATIVE_ABORT:
        nasm_push_instr(nasm, 2, "mov", "rax", "60");
        nasm_push_instr(nasm, 2, "mov", "rdi", "1");
        nasm_push_instr(nasm, 0, "syscall");
        break;
      default:
        assert(false && "Unreachable.");
      }
    } break;
    case INSTR_OR: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "or", "r10", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_AND: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "and", "r10", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_XOR: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "r11");
      nasm_push_instr(nasm, 1, "pop", "r10");
      nasm_push_instr(nasm, 2, "xor", "r10", "r11");
      nasm_push_instr(nasm, 1, "push", "r10");
    } break;
    case INSTR_SHL: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "rcx");
      nasm_push_instr(nasm, 1, "pop", "rbx");
      nasm_push_instr(nasm, 2, "shl", "rbx", "cl");
      nasm_push_instr(nasm, 1, "push", "rbx");
    } break;
    case INSTR_SHR: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 1, "pop", "rcx");
      nasm_push_instr(nasm, 1, "pop", "rbx");
      nasm_push_instr(nasm, 2, "shr", "rbx", "cl");
      nasm_push_instr(nasm, 1, "push", "rbx");
    } break;
    case INSTR_HLT: {
      PUSH_ADDR_LABEL(nasm, i);
      nasm_push_instr(nasm, 2, "mov", "rax", "60");
      nasm_push_instr(nasm, 2, "xor", "rdi", "rdi");
      nasm_push_instr(nasm, 0, "syscall");
    } break;
    default:
      assert(false && "Unreachable.");
    }
  }

  NASM_ASSEMBLE(nasm, output_file, "elf64");
}

void casm_transpile_bootstrap_asm(Nasm *nasm)
{
  nasm_push_text_generic(nasm, true, 2, "global", "_start");
  nasm_push_bss_generic(nasm, true, 3, "tmp:", "resb", "1");
  nasm_push_bss_generic(nasm, true, 3, "memory:", "resb", "640000");
  nasm_push_label(nasm, "dump");
  nasm_push_instr(nasm, 2, "mov", "r9", "-3689348814741910323");
  nasm_push_instr(nasm, 2, "sub", "rsp", "40");
  nasm_push_instr(nasm, 2, "mov", "BYTE [rsp+31]", "10");
  nasm_push_instr(nasm, 2, "lea", "rcx", "[rsp+30]");
  nasm_push_label(nasm, ".L2");
  nasm_push_instr(nasm, 2, "mov", "rax", "rdi");
  nasm_push_instr(nasm, 2, "lea", "r8", "[rsp+32]");
  nasm_push_instr(nasm, 1, "mul", "r9");
  nasm_push_instr(nasm, 2, "mov", "rax", "rdi");
  nasm_push_instr(nasm, 2, "sub", "r8", "rcx");
  nasm_push_instr(nasm, 2, "shr", "rdx", "3");
  nasm_push_instr(nasm, 2, "lea", "rsi", "[rdx+rdx*4]");
  nasm_push_instr(nasm, 2, "add", "rsi", "rsi");
  nasm_push_instr(nasm, 2, "sub", "rax", "rsi");
  nasm_push_instr(nasm, 2, "add", "eax", "48");
  nasm_push_instr(nasm, 2, "mov", "BYTE [rcx]", "al");
  nasm_push_instr(nasm, 2, "mov", "rax", "rdi");
  nasm_push_instr(nasm, 2, "mov", "rdi", "rdx");
  nasm_push_instr(nasm, 2, "mov", "rdx", "rcx");
  nasm_push_instr(nasm, 2, "sub", "rcx", "1");
  nasm_push_instr(nasm, 2, "cmp", "rax", "9");
  nasm_push_instr(nasm, 1, "ja", ".L2");
  nasm_push_instr(nasm, 2, "lea", "rax", "[rsp+32]");
  nasm_push_instr(nasm, 2, "mov", "edi", "1");
  nasm_push_instr(nasm, 2, "sub", "rdx", "rax");
  nasm_push_instr(nasm, 2, "xor", "eax", "eax");
  nasm_push_instr(nasm, 2, "lea", "rsi", "[rsp+32+rdx]");
  nasm_push_instr(nasm, 2, "mov", "rdx", "r8");
  nasm_push_instr(nasm, 2, "mov", "rax", "1");
  nasm_push_instr(nasm, 0, "syscall");
  nasm_push_instr(nasm, 2, "add", "rsp", "40");
  nasm_push_instr(nasm, 0, "ret");
  nasm_push_label(nasm, "strlen");
  nasm_push_instr(nasm, 2, "mov", "r12", "r10");
  nasm_push_instr(nasm, 2, "xor", "r11", "r11");
  nasm_push_label(nasm, ".L3");
  nasm_push_instr(nasm, 2, "cmp", "BYTE [r12]", "0");
  nasm_push_instr(nasm, 1, "je", ".L3_END");
  nasm_push_instr(nasm, 1, "inc", "r11");
  nasm_push_instr(nasm, 1, "inc", "r12");
  nasm_push_instr(nasm, 1, "jmp", ".L3");
  nasm_push_label(nasm, ".L3_END");
  nasm_push_instr(nasm, 0, "ret");
  nasm_push_label(nasm, "getchar");
  /*
	mov rax, 0
	mov rdi, 0
	mov rsi, tmp
	mov rdx, 1
	syscall
	mov al, BYTE [tmp]
	movzx rax, al
	push rax
   */
  nasm_push_instr(nasm, 2, "mov", "rax", "0");
  nasm_push_instr(nasm, 2, "mov", "rdi", "0");
  nasm_push_instr(nasm, 2, "mov", "rsi", "tmp");
  nasm_push_instr(nasm, 2, "mov", "rdx", "1");
  nasm_push_instr(nasm, 0, "syscall");
  nasm_push_instr(nasm, 2, "mov", "al", "BYTE [tmp]");
  nasm_push_instr(nasm, 2, "movzx", "rax", "al");
  nasm_push_instr(nasm, 0, "ret");
  nasm_push_label(nasm, "_start");
  nasm_push_instr(nasm, 2, "mov", "r15", "memory");
}
