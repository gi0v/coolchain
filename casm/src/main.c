#include <stdio.h>
#include <assert.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define NASMWRAP_IMPLEMENTATION
#include <nasm_wrap.h>
#undef NASMWRAP_IMPLEMENTATION

#define STRLIB_IMPLEMENTATION
#include <strlib.h>
#undef STRLIB_IMPLEMENTATION

#include <casm.h>
#include <common.h>
#include <string.h>
#include <cm_base.h>
#include <casm_ops.h>
#include <casm_labels.h>
#include <error_logger.h>
#include <casm_comments.h>
#include <casm_transpiler.h>

void usage(const char * const program_name, FILE * restrict stream, int exit_code)
{
  fprintf(stream, "USAGE: %s [OPTIONS] [INPUT].casm\n", program_name);
  fprintf(stream, "OPTIONS:\n");
  fprintf(stream, "    -h,                Displays this message.\n");
  fprintf(stream, "    -o [OUTPUT].cm,    Specifies the output file name.\n");
  fprintf(stream, "    -r,                Just runs the program, doesn't produce output file.\n");
  fprintf(stream, "    -nasm,             Transpiles the program to NASM syntax and assembles it.\n");

  exit(exit_code);
}

int main(int argc, char **argv)
{
  Cm *cm = NULL;
  Casm *casm = NULL;
  Nasm *nasm = NULL;
  bool just_run = false, free_output_file = false, transpile_to_nasm = false;
  const char *input_file = NULL;
  const char *output_file = NULL;
  const char * const program_name = shift(&argc, &argv);

  if (argc < 1) usage(program_name, stderr, 1);

  while (argc) {
    char *val = shift(&argc, &argv);

    if (val[0] == '-') {
      if (!strcmp(val, "-h")) usage(program_name, stdout, 0);
      else if (!strcmp(val, "-o")) output_file = shift(&argc, &argv);
      else if (!strcmp(val, "-r")) just_run = true;
      else if (!strcmp(val, "-nasm")) transpile_to_nasm = true;
      else log_args_error("invalid flag: `-%c`.", val[1]);
    } else {
      input_file = val;
    }
  }

  if (input_file == NULL)
    log_args_error("input file was not specified.");

  if (output_file == NULL) {
    String of_str;
    String if_str = str_from_static_cstr(input_file);
    size_t x;

    if (str_find_char_right(if_str, '.', &x))
      str_free(str_chop_right(&if_str, if_str.length - (x + 1)));

    of_str = if_str;
    of_str = str_append(of_str, ".cm");
    output_file = STR_CSTR(of_str);
    free_output_file = true;
  }

  nasm = nasm_init();

  casm = casm_new(input_file);
  casm_label_search(casm);
  casm_remove_comments(casm);
  casm_assemble_source(casm);

  if (just_run) {
    cm = cm_new();
    
    cm_load_program_from_array(cm, casm->bytecode, casm->bc_sz - 1);
    cm_exec(cm);
    cm_free(cm);
  } else if (transpile_to_nasm) casm_transpile_program(casm, nasm, output_file);
    else casm_write_program_to_file(casm, output_file);

  casm_free(casm);
  nasm_free(nasm);

  if (free_output_file)
    free((char *)output_file);

  return 0;
}
