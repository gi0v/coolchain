#include <casm_labels.h>

void casm_push_label(Casm *casm, String name)
{
  labels_table_add(casm->labels_table, name, casm->current_addr);
}

CmWord casm_get_label_addr(Casm *casm, String name)
{
  return labels_table_search(casm->labels_table, name);
}

void casm_label_search(Casm *casm)
{
  size_t i;

  for (i = 0; i < casm->src_split.count; ++i) {
    String str = casm->src_split.strings[i];
    char *s = STR_CSTR(str);

    if (s[0] == '%') { // Label identifier
      str = str_remove_char(str, '%');
      casm_push_label(casm, str);

      casm->src_split.strings[i] = str;
    } else casm->current_addr += 0x2;
  }
}
