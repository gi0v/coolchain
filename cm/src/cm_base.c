#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

#include <cm_base.h>
#include <error_logger.h>

static_assert(INSTRUCTION_COUNT == 38,
              "Instruction count changed, make sure to update the instructions definitions below.");

static const CmInstr instruction_defs[] = {
  [INSTR_PUSH] = { .type = INSTR_PUSH, .name = "push", .execute = exec_push, .has_param = true  },
  [INSTR_ADD]  = { .type = INSTR_ADD,  .name = "add",  .execute = exec_add,  .has_param = false },
  [INSTR_SUB]  = { .type = INSTR_SUB,  .name = "sub",  .execute = exec_sub,  .has_param = false },
  [INSTR_MUL]  = { .type = INSTR_MUL,  .name = "mul",  .execute = exec_mul,  .has_param = false },
  [INSTR_DIV]  = { .type = INSTR_DIV,  .name = "div",  .execute = exec_div,  .has_param = false },
  [INSTR_MOD]  = { .type = INSTR_MOD,  .name = "mod",  .execute = exec_mod,  .has_param = false },
  [INSTR_EQU]  = { .type = INSTR_EQU,  .name = "equ",  .execute = exec_equ,  .has_param = false },
  [INSTR_NEQ]  = { .type = INSTR_NEQ,  .name = "neq",  .execute = exec_neq,  .has_param = false },
  [INSTR_GRE]  = { .type = INSTR_GRE,  .name = "gre",  .execute = exec_gre,  .has_param = false },
  [INSTR_LES]  = { .type = INSTR_LES,  .name = "les",  .execute = exec_les,  .has_param = false },
  [INSTR_GEQ]  = { .type = INSTR_GEQ,  .name = "geq",  .execute = exec_geq,  .has_param = false },
  [INSTR_LEQ]  = { .type = INSTR_LEQ,  .name = "leq",  .execute = exec_leq,  .has_param = false },
  [INSTR_JMP]  = { .type = INSTR_JMP,  .name = "jmp",  .execute = exec_jmp,  .has_param = false },
  [INSTR_CJMP] = { .type = INSTR_CJMP, .name = "cjmp", .execute = exec_cjmp, .has_param = false },
  [INSTR_DUP]  = { .type = INSTR_DUP,  .name = "dup",  .execute = exec_dup,  .has_param = false },
  [INSTR_SWAP] = { .type = INSTR_SWAP, .name = "swap", .execute = exec_swap, .has_param = false },
  [INSTR_DROP] = { .type = INSTR_DROP, .name = "drop", .execute = exec_drop, .has_param = false },
  [INSTR_OVER] = { .type = INSTR_OVER, .name = "over", .execute = exec_over, .has_param = false },
  [INSTR_ROT]  = { .type = INSTR_ROT,  .name = "rot",  .execute = exec_rot,  .has_param = false },
  [INSTR_CALL] = { .type = INSTR_CALL, .name = "call", .execute = exec_call, .has_param = false },
  [INSTR_WR8]  = { .type = INSTR_WR8,  .name = "wr8",  .execute = exec_wr8,  .has_param = false },
  [INSTR_WR16] = { .type = INSTR_WR16, .name = "wr16", .execute = exec_wr16, .has_param = false },
  [INSTR_WR32] = { .type = INSTR_WR32, .name = "wr32", .execute = exec_wr32, .has_param = false },
  [INSTR_WR64] = { .type = INSTR_WR64, .name = "wr64", .execute = exec_wr64, .has_param = false },
  [INSTR_RD8]  = { .type = INSTR_RD8,  .name = "rd8",  .execute = exec_rd8,  .has_param = false },
  [INSTR_RD16] = { .type = INSTR_RD16, .name = "rd16", .execute = exec_rd16, .has_param = false },
  [INSTR_RD32] = { .type = INSTR_RD32, .name = "rd32", .execute = exec_rd32, .has_param = false },
  [INSTR_RD64] = { .type = INSTR_RD64, .name = "rd64", .execute = exec_rd64, .has_param = false },
  [INSTR_RET]  = { .type = INSTR_RET,  .name = "ret",  .execute = exec_ret,  .has_param = false },
  [INSTR_NOP]  = { .type = INSTR_NOP,  .name = "nop",  .execute = exec_nop,  .has_param = false },
  [INSTR_NAT]  = { .type = INSTR_NAT,  .name = "nat",  .execute = exec_nat,  .has_param = true  },
  [INSTR_NOT]  = { .type = INSTR_NOT,  .name = "not",  .execute = exec_not,  .has_param = false },
  [INSTR_OR]   = { .type = INSTR_OR,   .name = "or",   .execute = exec_or,   .has_param = false },
  [INSTR_AND]  = { .type = INSTR_AND,  .name = "and",  .execute = exec_and,  .has_param = false },
  [INSTR_XOR]  = { .type = INSTR_XOR,  .name = "xor",  .execute = exec_xor,  .has_param = false },
  [INSTR_SHL]  = { .type = INSTR_SHL,  .name = "shl",  .execute = exec_shl,  .has_param = false },
  [INSTR_SHR]  = { .type = INSTR_SHR,  .name = "shr",  .execute = exec_shr,  .has_param = false },
  [INSTR_HLT]  = { .type = INSTR_HLT,  .name = "hlt",  .execute = exec_hlt,  .has_param = false },
};

static CmNative cm_native_defs[] = {
  [NATIVE_DUMP]  = { .kind = NATIVE_DUMP,  .execute = exec_native_dump  },
  [NATIVE_WRITE] = { .kind = NATIVE_WRITE, .execute = exec_native_write },
  [NATIVE_READ]  = { .kind = NATIVE_READ,  .execute = exec_native_read  },
  [NATIVE_ABORT] = { .kind = NATIVE_ABORT, .execute = exec_native_abort },
};

static_assert(NATIVES_CAP == 4,
              "Instruction count changed, make sure to update the instructions definitions below.");

Cm *cm_new()
{
  Cm *cm;

  cm = calloc(1, sizeof(*cm));
  cm->natives = cm_native_defs;

  return cm;
}

void cm_free(Cm *cm)
{
  free(cm->program);
  free(cm);
}

void cm_exec(Cm *cm)
{
  cm->ip = -1;

  while (!cm->halt) {
    cm->ip++;
    cm->program[cm->ip].execute(cm, cm->program[cm->ip].parameter);
  }
}

void cm_load_program_from_array(Cm *cm, CmWord *code, size_t code_sz)
{
  cm->program = calloc(code_sz / 2, sizeof(*cm->program));

  size_t i, j;

  for (i = 0, j = 0; i < code_sz; i += 2, ++j) {
    cm->program[j] = instruction_defs[code[i]];
    cm->program[j].parameter = code[i + 1];
  }
}

void cm_load_program_from_file(Cm *cm, const char *i_f)
{
  FILE *fp;
  CmWord *program;
  size_t program_sz;

  fp = fopen(i_f, "rb");

  if (fp == NULL)
    log_io_error("could not open input file: %s", i_f);

  fseek(fp, 0, SEEK_END);
  program_sz = ftell(fp);
  rewind(fp);

  program = calloc(program_sz, sizeof(CmWord));

  fread(program, program_sz, sizeof(CmWord), fp);

  cm_load_program_from_array(cm, program, program_sz / sizeof(CmWord));
  
  fclose(fp);
  free(program);
}

CmInstrType get_cm_instr_type_from_str(const char *str)
{
  CmWord i, start, end;
  
  start = INSTR_PUSH;
  end = INSTR_HLT;

  for (i = start; i <= end; ++i)
    if (!strcmp(instruction_defs[i].name, str))
      return instruction_defs[i].type;

  log_cm_error("illegal instruction: %s", str);
  return 0;
}

CmInstr get_cm_instr_from_cm_instr_type(CmInstrType instr_t)
{
  return instruction_defs[instr_t];
}

CmWord cm_stack_pop(Cm *cm)
{
  CmWord ret;

  if (cm->sp == 0)
    log_cm_error("stack underflow (0x%llX).", cm->ip);

  ret = cm->stack[cm->sp - 1];
  cm->stack[cm->sp - 1] = 0;
  cm->sp--;

  return ret;
}

void exec_push(Cm *cm, CmWord param)
{
  if (cm->sp >= STACK_CAP)
    log_cm_error("stack overflow (0x%llX).", cm->ip);
  cm->stack[cm->sp++] = param;
}

void exec_add(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a + b);
}

void exec_sub(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a - b);
}

void exec_mul(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a * b);
}

void exec_div(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a / b);
}

void exec_mod(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a % b);
}

void exec_equ(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;
  
  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a == b);
}

void exec_neq(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;
  
  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a != b);
}

void exec_gre(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;
  
  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a > b);
}

void exec_les(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;
  
  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a < b);
}

void exec_geq(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;
  
  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a >= b);
}

void exec_leq(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;
  
  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a <= b);
}
  
void exec_jmp(Cm *cm, CmWord param)
{
  (void) param;

  CmAddr addr = cm_stack_pop(cm);

  if (addr == 0) cm->ip = 0;
  else cm->ip = (addr / 2) - 1;
}

void exec_cjmp(Cm *cm, CmWord param)
{
  (void) param;

  CmWord addr = cm_stack_pop(cm);

  if (cm_stack_pop(cm)) {
    if (addr == 0) cm->ip = 0;
    else cm->ip = ((CmAddr) addr / 2) - 1;
  }
}

void exec_dup(Cm *cm, CmWord param)
{
  (void) param;

  CmWord val = cm_stack_pop(cm);

  exec_push(cm, val);
  exec_push(cm, val);
}

void exec_swap(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a = cm_stack_pop(cm);
  CmWord b = cm_stack_pop(cm);

  exec_push(cm, a);
  exec_push(cm, b);
}

void exec_drop(Cm *cm, CmWord param)
{
  (void) param;

  cm_stack_pop(cm);
}

void exec_over(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a = cm_stack_pop(cm);
  CmWord b = cm_stack_pop(cm);

  exec_push(cm, b);
  exec_push(cm, a);
  exec_push(cm, b);
}

void exec_rot(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a = cm_stack_pop(cm);
  CmWord b = cm_stack_pop(cm);
  CmWord c = cm_stack_pop(cm);

  exec_push(cm, b);
  exec_push(cm, a);
  exec_push(cm, c);
}

void exec_nop(Cm *cm, CmWord param)
{ (void) cm; (void) param; }

void exec_call(Cm *cm, CmWord param)
{
  (void) param;

  CmAddr ip_tmp = cm->ip;
  exec_jmp(cm, 0);
  exec_push(cm, (ip_tmp + 1) * 2);

  return;
}

void exec_wr8(Cm *cm, CmWord param)
{
  (void) param;
  uint8_t value = (uint8_t) cm_stack_pop(cm);
  CmAddr addr = cm_stack_pop(cm);

  if (addr >= MEM_CAP)
    log_cm_error("illegal memory access (0x%llX).", cm->ip);

  cm->mem[addr] = value;
}

#define WRITE_BYTES(vm, type)                                  \
  do {                                                         \
    type value = (type) cm_stack_pop(vm);                      \
    CmAddr addr = cm_stack_pop(vm);                            \
    if (addr > (MEM_CAP - sizeof(type) - 1))                   \
      log_cm_error("illegal memory access (0x%llX).", vm->ip); \
    memcpy(&cm->mem[addr], &value,                             \
           sizeof(value));                                     \
  } while(false)                                               \

void exec_wr16(Cm *cm, CmWord param)
{
  (void) param;
  WRITE_BYTES(cm, uint16_t);
}

void exec_wr32(Cm *cm, CmWord param)
{
  (void) param;
  WRITE_BYTES(cm, uint32_t);
}

void exec_wr64(Cm *cm, CmWord param)
{
  (void) param;
  WRITE_BYTES(cm, uint64_t);
}

#define READ_BYTES(vm, type)                                   \
  do {                                                         \
    CmAddr addr = cm_stack_pop(vm);                            \
    if (addr >= MEM_CAP)                                       \
      log_cm_error("illegal memory access (0x%llX).", vm->ip); \
    type tmp;                                                  \
    memcpy(&tmp, &vm->mem[addr],                               \
           sizeof(type));                                      \
    exec_push(vm, tmp);                                        \
  } while (false)                                              \

void exec_rd8(Cm *cm, CmWord param)
{
  (void) param;
  READ_BYTES(cm, uint8_t);
}

void exec_rd16(Cm *cm, CmWord param)
{
  (void) param;
  READ_BYTES(cm, uint16_t);
}

void exec_rd32(Cm *cm, CmWord param)
{
  (void) param;
  READ_BYTES(cm, uint32_t);
}

void exec_rd64(Cm *cm, CmWord param)
{
  (void) param;
  READ_BYTES(cm, uint64_t);
}

void exec_ret(Cm *cm, CmWord param)
{
  (void) param;

  exec_jmp(cm, 0);
}

void exec_nat(Cm *cm, CmWord param)
{
  cm->natives[param].execute(cm);
}

void exec_native_dump(Cm *cm)
{
  printf("%lu\n", cm_stack_pop(cm));
}

void exec_native_write(Cm *cm)
{
  CmAddr addr = cm_stack_pop(cm);

  while (cm->mem[addr] != 0) {
    printf("%c", cm->mem[addr++]);
  }
}

void exec_native_read(Cm *cm)
{
  exec_push(cm, getchar());
}

void exec_native_abort(Cm *cm)
{
  (void) cm;

  abort(); // Critical bois.
}

void exec_not(Cm *cm, CmWord param)
{
  (void) param;

  CmWord word = cm_stack_pop(cm);

  exec_push(cm, !word);
}

void exec_or(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a | b);
}

void exec_and(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a & b);
}

void exec_xor(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a ^ b);
}

void exec_shl(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a << b);
}

void exec_shr(Cm *cm, CmWord param)
{
  (void) param;

  CmWord a, b;

  b = cm_stack_pop(cm);
  a = cm_stack_pop(cm);

  exec_push(cm, a >> b);
}

void exec_hlt(Cm *cm, CmWord param)
{
  (void) param;
  cm->halt = true;
}
