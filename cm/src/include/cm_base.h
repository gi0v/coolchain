#ifndef _CM_BASE_H
#define _CM_BASE_H

#define MEM_CAP 640000
#define STACK_CAP 1024

#include <stddef.h>
#include <string.h>
#include <stdbool.h>

#include <common.h>

enum CM_INSTR_TYPE {
  INSTR_PUSH = 0xA0, // Don't change this
  INSTR_ADD,
  INSTR_SUB,
  INSTR_MUL,
  INSTR_DIV,
  INSTR_MOD,
  INSTR_EQU,
  INSTR_NEQ,
  INSTR_GRE,
  INSTR_LES,
  INSTR_GEQ,
  INSTR_LEQ,
  INSTR_JMP,
  INSTR_CJMP,
  INSTR_DUP,
  INSTR_SWAP,
  INSTR_DROP,
  INSTR_OVER,
  INSTR_ROT,
  INSTR_NOP,
  INSTR_CALL,
  INSTR_RET,
  INSTR_WR8,
  INSTR_WR16,
  INSTR_WR32,
  INSTR_WR64,
  INSTR_RD8,
  INSTR_RD16,
  INSTR_RD32,
  INSTR_RD64,
  INSTR_NOT,
  INSTR_NAT,
  INSTR_OR,
  INSTR_AND,
  INSTR_XOR,
  INSTR_SHL,
  INSTR_SHR,
  INSTR_HLT, // Don't change this
};

#define INSTRUCTION_COUNT INSTR_HLT - INSTR_PUSH + 1

struct CM;

struct CM_INSTR {
  // Param
  CmWord parameter;

  // Metadata
  bool has_param;
  enum CM_INSTR_TYPE type;
  const char *name;

  // Functions
  void (*execute)(struct CM *cm, CmWord param);
};

enum CM_NATIVE_KIND {
  NATIVE_DUMP = 0, // Don't change this.
  NATIVE_WRITE,
  NATIVE_READ,
  NATIVE_ABORT, // Don't change this.
};

#define NATIVES_CAP NATIVE_ABORT + 1

struct CM_NATIVE {
  enum CM_NATIVE_KIND kind;

  void (*execute)(struct CM *cm);
};

struct CM {
  CmByte mem[MEM_CAP];
  CmWord stack[STACK_CAP];
  struct CM_INSTR *program;
  
  CmAddr sp;
  CmAddr ip;
  bool halt;

  struct CM_NATIVE *natives;
};

typedef enum CM_INSTR_TYPE CmInstrType;
typedef struct CM_INSTR CmInstr;
typedef struct CM Cm;
typedef struct CM_NATIVE CmNative;
typedef enum CM_NATIVE_KIND CmNativeKind;

Cm *cm_new();
void cm_free(Cm *cm);
void cm_exec(Cm *cm);
void cm_load_program_from_array(Cm *cm, CmWord *code, size_t code_sz);
void cm_load_program_from_file(Cm *cm, const char *i_f);
CmInstrType get_cm_instr_type_from_str(const char *str);
CmInstr get_cm_instr_from_cm_instr_type(CmInstrType instr_t);
CmWord cm_stack_pop(Cm *cm);
void exec_push(Cm *cm, CmWord param);
void exec_add(Cm *cm, CmWord param);
void exec_sub(Cm *cm, CmWord param);
void exec_mul(Cm *cm, CmWord param);
void exec_div(Cm *cm, CmWord param);
void exec_mod(Cm *cm, CmWord param);
void exec_equ(Cm *cm, CmWord param);
void exec_neq(Cm *cm, CmWord param);
void exec_gre(Cm *cm, CmWord param);
void exec_les(Cm *cm, CmWord param);
void exec_geq(Cm *cm, CmWord param);
void exec_leq(Cm *cm, CmWord param);
void exec_jmp(Cm *cm, CmWord param);
void exec_cjmp(Cm *cm, CmWord param);
void exec_dup(Cm *cm, CmWord param);
void exec_swap(Cm *cm, CmWord param);
void exec_drop(Cm *cm, CmWord param);
void exec_over(Cm *cm, CmWord param);
void exec_rot(Cm *cm, CmWord param);
void exec_nop(Cm *cm, CmWord param);
void exec_call(Cm *cm, CmWord param);
void exec_wr8(Cm *cm, CmWord param);
void exec_wr16(Cm *cm, CmWord param);
void exec_wr32(Cm *cm, CmWord param);
void exec_wr64(Cm *cm, CmWord param);
void exec_rd8(Cm *cm, CmWord param);
void exec_rd16(Cm *cm, CmWord param);
void exec_rd32(Cm *cm, CmWord param);
void exec_rd64(Cm *cm, CmWord param);
void exec_ret(Cm *cm, CmWord param);
void exec_nat(Cm *cm, CmWord param);
void exec_native_dump(Cm *cm);
void exec_native_write(Cm *cm);
void exec_native_read(Cm *cm);
void exec_native_abort(Cm *cm);
void exec_not(Cm *cm, CmWord param);
void exec_or(Cm *cm, CmWord param);
void exec_and(Cm *cm, CmWord param);
void exec_xor(Cm *cm, CmWord param);
void exec_shl(Cm *cm, CmWord param);
void exec_shr(Cm *cm, CmWord param);
void exec_hlt(Cm *cm, CmWord param);

#endif  // _CM_BASE_H
