#ifndef _ERROR_LOGGER_H
#define _ERROR_LOGGER_H

#include <stdarg.h>

#include <file_location.h>

void log_error_type(const char *type, const char *format, va_list args);
void log_args_error(const char *format, ...) __attribute__((noreturn));
void log_io_error(const char *format, ...) __attribute__((noreturn));
void log_cm_error(const char *format, ...) __attribute__((noreturn));
void log_assembling_error(const char *format, ...) __attribute__((noreturn));
void log_compiling_error(FileLocation fl, const char *format, ...) __attribute__((noreturn));

#endif  // _ERROR_LOGGER_H
