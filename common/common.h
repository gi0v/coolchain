#ifndef _COMMON_H
#define _COMMON_H

#include <stdint.h>

#define MAX_CM_WORD 0xFFFFFFFFFFFFFFFF

typedef uint8_t CmByte;
typedef uint64_t CmWord;
typedef CmWord CmAddr;

char *shift(int *argc, char ***argv);

#endif  // _COMMON_H
