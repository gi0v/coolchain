#ifndef _FILE_LOCATION_H
#define _FILE_LOCATION_H

typedef struct {
  const char *file;
  size_t row, col;
} FileLocation;

static inline FileLocation file_location_new(const char *fn) {
  return (FileLocation) { .row = 1, .col = 1, .file = fn };
}

#endif  // _FILE_LOCATION_H
