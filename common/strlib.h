/////////////////////////////////////////////////////////////////////////////////////
// MIT License                                                                     //
//                                                                                 //
// Copyright (c) 2022 Giovanni Mangiaro                                            //
//                                                                                 //
// Permission is hereby granted, free of charge, to any person obtaining a copy    //
// of this software and associated documentation files (the "Software"), to deal   //
// in the Software without restriction, including without limitation the rights    //
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell       //
// copies of the Software, and to permit persons to whom the Software is           //
// furnished to do so, subject to the following conditions:                        //
//                                                                                 //
// The above copyright notice and this permission notice shall be included in all  //
// copies or substantial portions of the Software.                                 //
//                                                                                 //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR      //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,        //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE     //
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER          //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,   //
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE   //
// SOFTWARE.                                                                       //
/////////////////////////////////////////////////////////////////////////////////////

#ifndef _STR_LIB_H

#define _STR_LIB_H

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

typedef struct {
  char *data;
  size_t length;
} String;

typedef struct {
  size_t count;
  String *strings;
} StringArray;

// IMPORTANT NOTE: The String generated from this macro can only be read,
// you can use it only for read-only operations.
// If the String you are creating will be modified in any way, you'll
// need to use either str_new(), str_from_static_cstr() or str_from_dynamic_cstr()
// and then free it with str_free() to avoid memory leaks.
#define STR(cstr) ((String) { .data = cstr, .length = sizeof(cstr) - 1 })

#define STR_CSTR(str) str.data
#define STR_LEN(str) str.length
#define STR_CMP(str1, str2) strcmp(STR_CSTR(str1), STR_CSTR(str2)) == 0
#define STR_CSTR_CMP(str, cstr) strcmp(STR_CSTR(str), cstr) == 0

#define STRFmt "%.*s"
#define STRArg(str) (int) (str).length, (str).data

#define FMT_APPEND_TO_STR(str, fmt, ...)	\
  do {						\
    char tmp[64];				\
    snprintf(tmp, 64, fmt, ##__VA_ARGS__);	\
    str = str_append(str, tmp);			\
  } while (false)				\

String str_new();
String str_from_static_cstr(const char *scstr);
String str_from_dynamic_cstr(char *dcstr);
void str_free(String str);
String str_prepend(String str, const char *cstr);
String str_prepend_char(String str, char c);
String str_append(String str, const char *cstr);
String str_append_char(String str, char c);
String str_remove_char(String str, size_t index);
bool str_find_char_left(String str, char c, size_t *out_i);
bool str_find_char_right(String str, char c, size_t *out_i);
bool str_contains_char(String str, char c);
char str_get_char(String str, size_t i);
String str_chop_left(String *str, size_t count);
String str_chop_right(String *str, size_t count);
bool str_read_file(String file_name, String *contents);
StringArray str_arr_new();
void str_arr_free(StringArray arr);
void simple_str_arr_free(StringArray arr);
StringArray str_arr_push(StringArray arr, String str);
StringArray str_split(String str, char delim);

#endif  // _STR_LIB_H

#ifdef STRLIB_IMPLEMENTATION

String str_new()
{ return (String) { .length = 1, .data = calloc(2, sizeof(char)) }; }

String str_from_static_cstr(const char *scstr)
{
  size_t i;
  String str;

  str.length = strlen(scstr) + 1;
  str.data = calloc(str.length + 1, sizeof(char));

  for (i = 0; i < str.length; ++i)
    str.data[i] = scstr[i];

  return str;
}

String str_from_dynamic_cstr(char *dcstr)
{ return (String) { .data = dcstr, .length = strlen(dcstr) + 1 }; }

void str_free(String str)
{
  if (str.data != NULL)
    free(str.data);
}

String str_prepend(String str, const char *cstr)
{
  char *tmp_ptr;
  char tmp, tmp2;
  size_t i, j;

  if (strlen(cstr) <= 0)
    return str;

  tmp_ptr = realloc(str.data, sizeof(char) * str.length + strlen(cstr) + 1);

  if (tmp_ptr == NULL) {
    free(str.data);
    return (String){0};
  } else str.data = tmp_ptr;

  for (i = 0; i < strlen(cstr); ++i) {
    tmp = str.data[i];

    for (j = i; j < str.length; ++j) {
      tmp2 = str.data[j + 1];
      str.data[j + 1] = tmp;
      tmp = tmp2;
    }

    str.length += 1;
    str.data[i] = cstr[i];
  }

  return str;
}

String str_prepend_char(String str, char c)
{
  size_t i;
  char *tmp_ptr;
  char tmp, tmp2;

  tmp_ptr = realloc(str.data, sizeof(char) * str.length + 2);

  if (tmp_ptr == NULL) {
    free(str.data);
    return (String){0};
  } else str.data = tmp_ptr;

  tmp = str.data[0];
  
  for (i = 0; i < str.length; ++i) {
    tmp2 = str.data[i + 1];
    str.data[i + 1] = tmp;
    tmp = tmp2;
  }
  
  str.length += 1;
  str.data[0] = c;

  return str;
}

String str_append(String str, const char *cstr)
{
  char *tmp_ptr;
  size_t i, prev_str_length;

  if (strlen(cstr) <= 0)
    return str;

  prev_str_length = str.length;
  str.length = str.length + strlen(cstr);
  tmp_ptr = realloc(str.data, sizeof(char) * (str.length + 1));

  if (tmp_ptr == NULL) {
    free(str.data);
    return (String){0};
  } else str.data = tmp_ptr;

  for (i = prev_str_length - 1; i < str.length; ++i)
    str.data[i] = cstr[i - prev_str_length + 1];

  str.data[i] = 0;

  return str;
}

String str_append_char(String str, char c)
{
  char *tmp_ptr;
  str.length++;
  tmp_ptr = realloc(str.data, sizeof(char) * (str.length + 1));

  if (tmp_ptr == NULL) {
    free(str.data);
    return (String){0};
  } else str.data = tmp_ptr;

  str.data[str.length - 2] = c;
  str.data[str.length - 1] = 0;

  return str;
}

String str_remove_char(String str, size_t index)
{
  size_t i;
  char *s = STR_CSTR(str);

  if (index > str.length - 1)
    index = str.length - 1;

  for (i = index; i < str.length - 1; ++i)
    s[i] = s[i + 1];

  s[str.length - 1] = 0;

  str.length--;

  return str;
}

bool str_find_char_left(String str, char c, size_t *out_i)
{
  size_t org_i = *out_i;
  char *s = STR_CSTR(str);

  for (*out_i = 0; *out_i < str.length; ++(*out_i))
    if (s[*out_i] == c)
      return true;

  *out_i = org_i;

  return false;
}

bool str_find_char_right(String str, char c, size_t *out_i)
{
  size_t org_i = *out_i;
  char *s = STR_CSTR(str);

  for (*out_i = str.length - 1; *out_i + 1 > 0; --(*out_i))
    if (s[*out_i] == c)
      return true;

  *out_i = org_i;

  return false;
}

bool str_contains_char(String str, char c)
{
  size_t i;

  for (i = 0; i < str.length; ++i)
    if (str.data[i] == c)
      return true;

  return false;
}

char str_get_char(String str, size_t i)
{
  if (i >= str.length)
    return 0;

  return str.data[i];
}

String str_chop_left(String *str, size_t count)
{
  size_t i;
  String res = str_new();

  if (count > str->length)
    count = str->length;

  for (i = 0; i < count; ++i) {
    (void) i;
    res = str_append_char(res, str->data[0]);
    *str = str_remove_char(*str, 0);
  }

  return res;
}

String str_chop_right(String *str, size_t count)
{
  size_t i;
  String res = str_new();

  if (count > str->length)
    count = str->length;
  
  for (i = 0; i < count; ++i) {
    (void) i;
    res = str_prepend_char(res, str->data[str->length - 2]);
    *str = str_remove_char(*str, str->length - 2);
  }
  
  return res;
}

StringArray str_arr_new()
{
  StringArray arr;

  arr.count = 0;
  arr.strings = calloc(1, sizeof(String));

  return arr;
}

// NOTE: This frees all the strings in that array too.
// You *may* want to call simple_str_arr_free() instead (which doesn't do that).
void str_arr_free(StringArray arr)
{
  size_t i;

  for (i = 0; i < arr.count; ++i)
    str_free(arr.strings[i]);

  free(arr.strings);
}

void simple_str_arr_free(StringArray arr)
{
  free(arr.strings);
}

StringArray str_arr_push(StringArray arr, String str)
{
  String *tmp_ptr = realloc(arr.strings, sizeof(String) * (arr.count + 2));

  if (tmp_ptr == NULL) {
    str_arr_free(arr);
    return (StringArray){0};
  } else arr.strings = tmp_ptr;

  arr.strings[arr.count] = str;
  arr.count++;

  return arr;
}

StringArray str_split(String str, char delim)
{
  size_t i;
  String to_push;
  StringArray arr;

  arr = str_arr_new();
  to_push = str_new();

  for (i = 0; i < str.length; ++i) {
    if (str.data[i] == delim) {
      arr = str_arr_push(arr, to_push);
      to_push = str_new();
    } else to_push = str_append_char(to_push, str.data[i]);
  }

  arr = str_arr_push(arr, to_push);

  return arr;
}

bool str_read_file(String file_name, String *contents)
{
  FILE *fp;
  char *buf;
  size_t length;

  fp = fopen(STR_CSTR(file_name), "r");

  if (fp == NULL)
    return false;

  if (fseek(fp, 0, SEEK_END) < 0)
    return false;

  length = ftell(fp);

  if (length <= 0)
    return false;

  rewind(fp);

  if ((buf = calloc(length, sizeof(char))) == NULL)
    return false;

  if (length != (contents->length = fread(buf, sizeof(char), length, fp)))
    { free(buf); return false; }

  if (ferror(fp))
    { free(buf); return false; }

  contents->data = buf;
  fclose(fp);

  return true;
}

#endif  // STRLIB_IMPLEMENTATION
