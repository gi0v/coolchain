#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include <strlib.h>
#include <file_location.h>

void log_error_type(const char *type, const char *format, va_list args)
{
  fprintf(stderr, "%s: error: ", type);
  vfprintf(stderr, format, args);
  va_end(args);
  fprintf(stderr, "\n");

  exit(1); // => Error.
}

void log_args_error(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  log_error_type("sysargs", format, args);
}

void log_io_error(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  log_error_type("sysio", format, args);
}

// Custom log errors

void log_cm_error(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  log_error_type("cm", format, args);
}

void log_assembling_error(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  log_error_type("casm", format, args);
}

void log_compiling_error(FileLocation fl, const char *format, ...)
{
  String str = str_from_static_cstr(fl.file);

  str = str_append_char(str, ':');
  FMT_APPEND_TO_STR(str, "%zu", fl.row);
  str = str_append_char(str, ':');
  FMT_APPEND_TO_STR(str, "%zu", fl.col);

  va_list args;
  va_start(args, format);
  log_error_type(STR_CSTR(str), format, args);

  str_free(str);
}
