#include <assert.h>

#include <common.h>

char *shift(int *argc, char ***argv)
{
  assert(*argc > 0);

  char *ret = **argv;

  *argc -= 1;
  *argv += 1;

  return ret;
}
