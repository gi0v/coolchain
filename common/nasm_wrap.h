#ifndef _NASM_WRAP_H
#define _NASM_WRAP_H

#include <stdio.h>
#include <stdarg.h>

#include "./strlib.h"

typedef struct {
  String src_text;
  String src_data;
  String src_bss;
} Nasm;

#define RUN_CMD(cmd, output)                                            \
  printf("[CMD] %s\n", cmd);                                            \
  pp = popen(cmd, "r");                                                 \
  if (pp == NULL) {                                                     \
    printf("[ERROR] Failed to run nasm command.\n");                    \
    exit(1);                                                            \
  }                                                                     \
  while (fgets(output, sizeof(output), pp))                             \
    printf("%s", output);                                               \
  pclose(pp);                                                           \
  
#define NASM_ASSEMBLE(nasm, out, arch)                                  \
  do {                                                                  \
    String cmd = str_new();                                             \
    FILE *fp, *pp;                                                      \
    char output[1024];                                                  \
    fp = fopen(".__nasm_wrap_tmp.asm", "w+");                           \
    nasm_dump_source(nasm, fp);                                         \
    fclose(fp);                                                         \
    cmd = str_append(cmd, "nasm -f");                                   \
    cmd = str_append(cmd, arch);                                        \
    cmd = str_append(cmd, " -o ");                                      \
    cmd = str_append(cmd, out);                                         \
    cmd = str_append(cmd, ".o .__nasm_wrap_tmp.asm");                   \
    RUN_CMD(STR_CSTR(cmd), output);                                     \
    str_free(cmd);                                                      \
    cmd = str_new();                                                    \
    cmd = str_append(cmd, "ld -o ");                                    \
    cmd = str_append(cmd, out);                                         \
    cmd = str_append(cmd, " ");                                         \
    cmd = str_append(cmd, out);                                         \
    cmd = str_append(cmd, ".o");                                        \
    RUN_CMD(STR_CSTR(cmd), output);                                     \
    str_free(cmd);                                                      \
    cmd = str_new();                                                    \
    cmd = str_append(cmd, "rm -f .__nasm_wrap_tmp.asm ");               \
    cmd = str_append(cmd, out);                                         \
    cmd = str_append(cmd, ".o");                                        \
    RUN_CMD(STR_CSTR(cmd), output);                                     \
    str_free(cmd);                                                      \
  } while (false)                                                       \

Nasm *nasm_init();
void nasm_free(Nasm *nasm);
void nasm_push_preproc_instr(Nasm *nasm, size_t args_count, const char *instr_name, ...);
void nasm_push_bss_generic(Nasm *nasm, bool newline, size_t args_count, ...);
void nasm_push_data_generic(Nasm *nasm, bool newline, size_t args_count, ...);
void nasm_push_text_generic(Nasm *nasm, bool newline, size_t args_count, ...);
void nasm_push_label(Nasm *nasm, const char *name);
void nasm_push_instr(Nasm *nasm, size_t args_count, const char *instr_name, ...);
void nasm_dump_source(Nasm *nasm, FILE *stream);

#endif  // _NASM_WRAP_H

#ifdef NASMWRAP_IMPLEMENTATION

Nasm *nasm_init()
{
  Nasm *nasm = calloc(1, sizeof(*nasm));

  nasm->src_text = str_new();
  nasm->src_data = str_new();
  nasm->src_bss  = str_new();

  nasm->src_text = str_append(nasm->src_text, "section .text\n");
  nasm->src_data = str_append(nasm->src_data, "section .data\n");
  nasm->src_bss = str_append(nasm->src_bss, "section .bss\n");

  return nasm;
}

void nasm_free(Nasm *nasm)
{
  str_free(nasm->src_text);
  str_free(nasm->src_data);
  str_free(nasm->src_bss);
  free(nasm);
}

void nasm_push_preproc_instr(Nasm *nasm, size_t args_count, const char *instr_name, ...)
{
  size_t i;
  va_list instr_args;

  va_start(instr_args, instr_name);

  nasm->src_text = str_append_char(nasm->src_text, '%');
  nasm->src_text = str_append(nasm->src_text, instr_name);

  if (args_count <= 0) goto eol;

  nasm->src_text = str_append_char(nasm->src_text, ' ');

  for (i = 0; i < args_count; ++i) {
    nasm->src_text = str_append(nasm->src_text, va_arg(instr_args, const char *));
    nasm->src_text = str_append_char(nasm->src_text, ' ');
  }

 eol:
  nasm->src_text = str_append_char(nasm->src_text, '\n');

  va_end(instr_args);
}

void nasm_push_bss_generic(Nasm *nasm, bool newline, size_t args_count, ...)
{
  size_t i;
  va_list instr_args;

  va_start(instr_args, args_count);

  if (args_count <= 0) goto eol;

  for (i = 0; i < args_count; ++i) {
    nasm->src_bss = str_append(nasm->src_bss, va_arg(instr_args, const char *));
    nasm->src_bss = str_append_char(nasm->src_bss, ' ');
  }

 eol:
  if (newline)
    nasm->src_bss = str_append_char(nasm->src_bss, '\n');

  va_end(instr_args);
}

void nasm_push_data_generic(Nasm *nasm, bool newline, size_t args_count, ...)
{
  size_t i;
  va_list instr_args;

  va_start(instr_args, args_count);

  if (args_count <= 0) goto eol;

  for (i = 0; i < args_count; ++i) {
    nasm->src_data = str_append(nasm->src_data, va_arg(instr_args, const char *));
    nasm->src_data = str_append_char(nasm->src_data, ' ');
  }

 eol:
  if (newline)
    nasm->src_data = str_append_char(nasm->src_data, '\n');

  va_end(instr_args);
}

void nasm_push_text_generic(Nasm *nasm, bool newline, size_t args_count, ...)
{
  size_t i;
  va_list instr_args;

  va_start(instr_args, args_count);

  if (args_count <= 0) goto eol;

  for (i = 0; i < args_count; ++i) {
    nasm->src_text = str_append(nasm->src_text, va_arg(instr_args, const char *));
    nasm->src_text = str_append_char(nasm->src_text, ' ');
  }

 eol:
  if (newline)
    nasm->src_text = str_append_char(nasm->src_text, '\n');

  va_end(instr_args);
}

void nasm_push_label(Nasm *nasm, const char *name)
{
  nasm->src_text = str_append(nasm->src_text, name);
  nasm->src_text = str_append(nasm->src_text, ":\n");
}

void nasm_push_instr(Nasm *nasm, size_t args_count, const char *instr_name, ...)
{
  size_t i;
  va_list instr_args;

  va_start(instr_args, instr_name);

  nasm->src_text = str_append_char(nasm->src_text, '\t');
  nasm->src_text = str_append(nasm->src_text, instr_name);

  if (args_count <= 0) goto eol;

  nasm->src_text = str_append_char(nasm->src_text, ' ');

  for (i = 0; i < args_count - 1; ++i) {
    nasm->src_text = str_append(nasm->src_text, va_arg(instr_args, const char *));
    nasm->src_text = str_append_char(nasm->src_text, ',');
    nasm->src_text = str_append_char(nasm->src_text, ' ');
  }

  nasm->src_text = str_append(nasm->src_text, va_arg(instr_args, const char *));

 eol:
  nasm->src_text = str_append_char(nasm->src_text, '\n');

  va_end(instr_args);
}

void nasm_dump_source(Nasm *nasm, FILE *stream)
{
  fprintf(stream, ";;; --- NASM_WRAP GENERATED SRC START --- ;;;\n");

  fprintf(stream, STRFmt, STRArg(nasm->src_text));
  fprintf(stream, STRFmt, STRArg(nasm->src_data));
  fprintf(stream, STRFmt, STRArg(nasm->src_bss));

  fprintf(stream, ";;; ---  NASM_WRAP GENERATED SRC END  --- ;;;\n");
}

#endif // NASMWRAP_IMPLEMENTATION
